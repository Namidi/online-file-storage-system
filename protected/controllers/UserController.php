<?php

class UserController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public $user_id;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'admin', 'view', 'create', 'update', 'delete', 'contact'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny', // deny all users
                'actions' => array('admin', 'create', 'delete', 'index', 'view'),
                'expression' => '!Yii::app()->user->isAdmin()'
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
//        $this->layout='//layouts/theme_column2';
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    
    public function getRoles(){
        return array(
            1 => 'Admin',
            2 => 'Docent',
            3 => 'Student'
        );
    }
    
    public function generatePassword() {
        $password = '';
        $characters = '0123456789aAbBcCdDeEfFgGhHiIjJkKlLmMnNOpPqQrRsStTuUvVwWXyYzZ';
        for ($p = 0; $p < 8; $p++) {
            $password .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $password;
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;
        // Uncomment the following line if AJAX validation is needed 
        // $this->performAjaxValidation($model);


        if (isset($_FILES['import_list']) && $_FILES['import_list']['error'] == UPLOAD_ERR_OK) {
            
            if (pathinfo($_FILES['import_list']['name'], PATHINFO_EXTENSION) !== 'csv') {
                $model->addError('wrong_mime_type', "The download file must be \".csv\"");
                $this->render('create', array(
                    'model' => $model,
                    'roles' => $this->getRoles()
                ));
                Yii::app()->end();
            }
            if ($_FILES['import_list']['name'] != ('Docents Email.csv' || 'Students Email.csv')) {
                $model->addError('wrong_file_name', "The download file name must be \"Docents Email\" or \"Students Email\"");
                $this->render('create', array(
                    'model' => $model,
                    'roles' => $this->getRoles()
                ));
                Yii::app()->end();
            }

            switch ($_FILES['import_list']['name']) {
                case 'Docents Email.csv':
                    $user_role = 2;
                    break;
                case 'Students Email.csv':
                default:
                    $user_role = 3;
                    break;
            }
            
            $handle = fopen($_FILES['import_list']['tmp_name'], 'r');
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
//                echo "<pre>";
//                    print_r($data);
//                echo "</pre>";
//                continue;
                if (!$data[0] || !filter_var($data[0], FILTER_VALIDATE_EMAIL)) {
                    continue;
                }
                if (User::model()->findByAttributes(array('email' => $data[0]))) {
                    continue;
                }
                
                $model->id = false;
                $model->isNewRecord = true;
                $data[0] = trim($data[0]);
                $model->email = $data[0];
                $model->role = $user_role;
                $model->username = explode('@', $model->email);
                $model->username = $model->username[0];
                $password = $this->generatePassword();
                $model->password = md5($password);

                if ($model->save(false)) {
                    $mail = new YiiMailer();
                    $mail->clearLayout(); //if layout is already set in config
                    $mail->setFrom('unasat@unasat.org', 'UNASAT');
                    $mail->setTo($model->email);
                    $mail->setSubject('Succesvolle Registratie UNASAT OFSS');
                    $mail->setBody('Beste gebruiker, <br /><br /> Hierbij ontvangt u de inlog gegevens voor het UNASAT OFSS. <br /> <b>Login Email Address:</b> ' . $model->email . ' <br /> <b>Password:</b> ' . $password . ' <br /> Het UNASAT OFSS kunt u bezoeken via deze link: <a href="http://www.unasat.org/inlogofss.html">http://www.unasat.org/inlogofss.html</a><br /><br /> Mvg, <br /><br /> Het UNASAT Team!');
                    $mail->send();
                }
            }
//            exit;
            $this->redirect(array('user/admin'));
        }



        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];
            $model->username = explode('@', $model->email);
            $model->username = $model->username[0];
            $password = $this->generatePassword();
            $model->password = md5($password);
            if ($model->save()) {
                
                $mail = new YiiMailer();
                $mail->setView('registration');
                $mail->setData(array(
                        'from' => 'UNASAT admin.secr@unasat.sr',
                        'to' => $model->email,
                        'subject' => 'Succesvolle Registratie UNASAT OFSS',
                        'email' => $model->email,
                        'password' => $password
                    ));
                $mail->setLayout('mail');
                $mail->setFrom('unasat@unasat.org', 'UNASAT');
                $mail->setTo($model->email);
                $mail->setSubject('Succesvolle Registratie UNASAT OFSS');
                $mail->send();
                
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $this->render('create', array(
            'model' => $model,
            'roles' => $this->getRoles()
        ));
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
//        $this->layout = '//layouts/theme_column2';
        $model = $this->loadModel($id);

        $this->user_id = Yii::app()->user->getId();
        if($this->user_id == 1 || $this->user_id == 136 || $this->user_id == 148 ){
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];

                if ($model->validate()){

                    if (!empty($model->password)){
                        $password = '<b>Password:</b> '.$model->password;
                        $model->password = User::hashPassword($model->password);
                    }else{
                        $password = 'Use old password';
                        unset($model->password);
                    }

                    if($model->save(false));{
                        $mail = new YiiMailer();
                        $mail->clearLayout();//if layout is already set in config
                        $mail->setFrom('unasat@unasat.org', 'UNASAT');
                        $mail->setTo($model->email);
                        $mail->setSubject('Succesvolle Update UNASAT OFSS');
                        $mail->setBody('Beste gebruiker, <br /><br /> Hierbij ontvangt u de herziene inlog gegevens voor het UNASAT OFSS. <br /> <b>Login Email Address:</b> '.$model->email.'<br />'.$password.'<br /><br /> Mvg, <br /><br /> Het UNASAT Team!');
                        $mail->send();
                        Yii::app()->user->setFlash('success', 'Profile has been updated!');
                        $this->redirect(array('update', 'id' => $model->id));
                    }
                }
            }
            $model->password = '';
            $model->new_password = '';
            $this->render('update', array(
                'model' => $model,
                'roles' => $this->getRoles()
            ));

        } else if($this->user_id == NULL || $this->user_id != $id) {
            $this->redirect(Yii::app()->homeUrl);
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
//        if (isset($_POST['User'])) {
//            $model->attributes = $_POST['User'];
//
//            if ($model->validate()){
//
//                 if (!empty($model->password)){
//                    $password = '<b>Password:</b> '.$model->password;
//                    $model->password = User::hashPassword($model->password);
//                 }else{
//                     $password = 'Use old password';
//                     unset($model->password);
//                 }
//
//                if($model->save(false));{
//                    $mail = new YiiMailer();
//                    $mail->clearLayout();//if layout is already set in config
//                    $mail->setFrom('unasat@unasat.org', 'UNASAT');
//                    $mail->setTo($model->email);
//                    $mail->setSubject('Succesvolle Update UNASAT OFSS');
//                    $mail->setBody('Beste gebruiker, <br /><br /> Hierbij ontvangt u de herziene inlog gegevens voor het UNASAT OFSS. <br /> <b>Login Email Address:</b> '.$model->email.'<br />'.$password.'<br /><br /> Mvg, <br /><br /> Het UNASAT Team!');
//                    $mail->send();
//                    Yii::app()->user->setFlash('success', 'Profile has been updated!');
//                    $this->redirect(array('update', 'id' => $model->id));
//                }
//            }
//        }
//        $model->password = '';
//        $model->new_password = '';
//        $this->render('update', array(
//            'model' => $model,
//            'roles' => $this->getRoles()
//        ));
    }
    

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
				
			$this->loadModel($id)->delete();	
				
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				/* if(!isset($_GET['ajax']))
						$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('dataManagement')); */
		}
		else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

       

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        /*if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));*/
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
//        $this->layout='//layouts/theme_column2';
        $dataProvider = new CActiveDataProvider('User');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
//        $this->layout='//layouts/theme_column2';
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
