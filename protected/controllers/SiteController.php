<?php

class SiteController extends Controller {
	
    /**
     * Declares class-based actions.
     */
	public $root;
	public $user_id;
	public $root_langth;
    public function actions() {
        
		return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
            'connector' => array(
                'class' => 'ext.elFinder.ElFinderConnectorAction',
                'settings' => array(
                    'bind' => array(
                        'mkdir upload rm paste rename' => 'customCallback'
                    ),
                    'roots' => array(
                        array(
                            'driver' => 'LocalFileSystem',
                            'path' => Yii::getPathOfAlias('webroot') . '/filestorage/',
                            'URL' => Yii::app()->baseUrl . '/filestorage/',
                            'acceptedName' => 'customValidName',
                            'attributes'=>$this->getElfinderAttributes(),
//                          'allowedExtensions' => 'swf,flv,mov,mp4',
//                          'deniedExtensions' => 'jpg',
                        )
                    )
                )
            )
        );
    }

    public function accessRules() {

//                    echo "<pre>";
//                        print_r(Yii::app()->user->isAdmin());
//                    echo "</pre>";
//                    die();
        return array(
            array('deny',
                'actions' => array('index', 'contact'),
                'expression' => 'Yii::app()->user->isGuest'
            ),
            array('deny', // deny all users
                'actions' => array('contact'),
                'expression' => '!Yii::app()->user->isGuest'
            ),
//          array('allow', // allow all users to perform 'index' and 'view' actions
//          	'actions' => array('index'),
//             	'expression' => 'Yii::app()->user->isAdmin()',
//          )
        );
    }
    
	
	
	
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex(){
		
	
		$this->user_id = Yii::app()->user->getId();
		$attachmentsModel = new Attachments;
		
		
		/*$a = urlencode('hhh   hhh &() $#@dhghh.,??');
		var_dump($a);echo urldecode($a);exit;*/
		
		$userModel = new User;
		$user = $userModel->findByPk($this->user_id);
		$user_role = $user->role;
		
		if (!function_exists('prm_students')) {
			function prm_students($node){
				if(isset($node)){
					$arr = explode('/', $node);
					//var_dump($arr);var_dump(count($arr));exit;
					for($i=0;$i<count($arr);$i++){
						if($arr[$i] == "Student Uploads"){
							return TRUE;
							break;
						}
						
					}
				}
			}
		}
		
		
		
		
		$user_uploads = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR . 'home' .DIRECTORY_SEPARATOR;
		if(isset($_POST['action'])) {
			$fs = new JstreeLib($user_uploads);
			try {
				$rslt = null;
				switch($_POST['action']) {
					case 'home':
						if(is_dir($user_uploads)){
							$dir_contine_ = scandir($user_uploads);
							$dir_contine = "";
							foreach($dir_contine_ as $k=>$v){
								if($v != '.' && $v != '..'){
									$dir_contine .= "'".urldecode($v)."'".",";
								
								}
							
							}
							$dir_contine = rtrim($dir_contine,',');
							//var_dump(rtrim($dir_contine,','));exit;
							if($dir_contine != ""){
								$rslt = $attachmentsModel->select_dirContine($dir_contine);
								if(!empty($rslt)){
									foreach($rslt as $v){
											$file_name = end(explode('/',$v['path']));
											$file_name_dir = urlencode($file_name);
											$lst[] = array(
														'path' => $file_name,
														'user_id' => $v['user_id'],
														'size' => $v['size'],
														'type' => $v['type'],
														'date' => $v['date'],
														'name' => $file_name,
														'url'=>$file_name_dir
														
													);
									}
									echo json_encode($lst);exit;
								}
							}
						}
						break;
					case 'open_dir':
						if(isset($_POST['id'])){
							$file_id = $_POST['id'];
							$dir = $file_id == '/'?$user_uploads:$user_uploads.$file_id;
							
							if(is_dir($dir)){
								$dir_contine_ = scandir($dir);
								//var_dump($dir_contine_);exit;
								$dir_contine = "";
								foreach($dir_contine_ as $k=>$v){
									if($v != '.' && $v != '..'){
										if($file_id != '/'){
											$dir_contine .= "'".$file_id.'/'.$v."'".",";
										}else{
											$dir_contine .= "'".$v."'".",";
										}
									}
								
								}
								$dir_contine = rtrim($dir_contine,',');
								$std = Yii::app()->user->isStudent()?true:false;
								if($dir_contine != ""){
									$rslt = $attachmentsModel->selectStdId($this->user_id,$dir_contine,$std);
									if(!empty($rslt)){
										foreach($rslt as $v){
											$file_name = end(explode('/',$v['path']));
											$file_name_dir = urlencode($v['path']);
											$lst[] = array(
														'path' => $file_id != '/'?$file_id.'/'.$file_name:$file_name,
														'user_id' => $v['user_id'],
														'size' => $v['size'],
														'type' => $v['type'],
														'date' => $v['date'],
														'name' => $file_name,
														'url'=> $file_name_dir //$file_id != '/'?$file_id.'/'.$file_name_dir:$file_name_dir
														
													);
										}
										echo json_encode($lst);exit;
									}else{
										$rslt = null;
										echo json_encode($rslt);exit;
									}
								}
							}
						
						}
					
						break;
					case 'remove':
						if(isset($_POST['id']) && $_POST['id'] != ""){
							$file_id = $_POST['id'];
							$user_id = $attachmentsModel->select_user_id($file_id);
							if(Yii::app()->user->isAdmin() || (isset($user_id['user_id']) && $user_id['user_id'] == Yii::app()->user->getId()) ){
								$res = $fs->remove($file_id);
								$attachmentsModel = new Attachments;
								$attachmentsModel->delete($file_id);
								$rslt = true;
							}
						}
						break;
					case 'rename':
						if(isset($_POST['id']) && $_POST['id'] != "" && isset($_POST['name']) && $_POST['name'] != ""){
							$file_id = $_POST['id'];
							$name = $_POST['name'];
							$user_id = $attachmentsModel->select_user_id($file_id);
							if(Yii::app()->user->isAdmin() || (isset($user_id['user_id']) && $user_id['user_id'] == Yii::app()->user->getId()) ){
								$sp_file_id  = explode("/", $file_id);
								
								if(count($sp_file_id) != 1){
									$i = count($sp_file_id) - 2;
									$f_id = $sp_file_id[$i];
									$result = $attachmentsModel->select_user_id($f_id.'/'.$name);
								}else{
									$result = $attachmentsModel->select_user_id($name);
								}
				
								if(empty($result)){
									if( isset($user_id['type']) && $user_id['type'] != 0 ){
										$type = end(explode(".", $user_id['path']));
										$n_type = end(explode(".", $name));
										if($type != $n_type){
											$res_type = false;
										}else{
											$res_type = true;
										}
									}else{
										$res_type = true;
									}
									if($res_type){
										$rslt = $fs->rename($file_id, isset($_POST['name']) ? $_POST['name'] : '');
										
										$find = $file_id;
										$replace = $rslt['id'];
						
										$model = Attachments::model()->findAll(array(
											'condition' => 'path LIKE :match OR path =:node',
											'params' => array(
												':match' => $find.'/'.'%',
												':node' => $find
											)
										));
										//var_dump($model);exit;
										foreach($model as $key=>$item){
											$item->path = $this->str_coustm_replace($item->path,$find,$replace);//str_replace($find, $replace, $item->path);
											
											$item->save();
										}
										//var_dump($replace);exit;
										$rslt = array(
														'res' =>urlencode($replace),
														'res_id' =>$replace
													);	
									}
								}	
							}
						}
						break;
					case 'create_new_folder':
						$node = isset($_POST['id']) && $_POST['id'] !== '#' ? $_POST['id'] : '/';
						if(!Yii::app()->user->isStudent() || prm_students($node)){
							$f_name = 'Untitled Folder';
							if($node != "/"){
								$name = $attachmentsModel->exist_fileName_model($node.'/'.$f_name);
							}else{
								$name = $attachmentsModel->exist_fileName_model($f_name);
							}
							$num = 1;
							while(!empty($name)) {
								
								$n_name = 'Untitled Folder '.$num;
								if($node != "/"){
									$name = $attachmentsModel->exist_fileName_model($node.'/'.$n_name);
								}else{
									$name = $attachmentsModel->exist_fileName_model($n_name);
								}
								//$name = $attachmentsModel->exist_fileName_model($file_id.'/'.$n_name);
								$f_name = $n_name;
								$num = $num + 1;
							}
							//var_dump($node);var_dump($f_name);exit;
							$rslt = $fs->create($node, $f_name,true);
							$attachmentsModel = new Attachments;
							//$res_exist = $attachmentsModel->nodeExist($node);
		
							
								$attachmentsModel->path = $rslt['id'];
								$attachmentsModel->user_id = $this->user_id;
								$attachmentsModel->date = date('c',time());
								$attachmentsModel->type = 0;
								$attachmentsModel->save(false);
							//}
							
							if($node != "/"){
									$name = $attachmentsModel->exist_fileName_model($node.'/'.$f_name);
							}else{
									$name = $attachmentsModel->exist_fileName_model($f_name);
							}
							if(!empty($name)){
								$rslt = $name; 
							}else{
								$rslt = false;
							}
						}else{
							$rslt = false;
						}
							
					
						break;
					default:
						throw new Exception('Unsupported operation: ' . $_POST['action']);
						break;
				}
				//header('Content-Type: application/json; charset=utf-8');
				echo json_encode($rslt);
			}
			catch (Exception $e) {
				header($_SERVER["SERVER_PROTOCOL"] . ' 500 Server Error');
				header('Status:  500 Server Error');
				echo $e->getMessage();
			}
			die();	
		}
		
		
		if (!file_exists($user_uploads)) {
			mkdir($user_uploads, 0755, true);
		}
		$user_uploaded_size = $this->foldersize($user_uploads);
		//if(isset($_POST['show_file'])){
			$shortcode_tags = array();
			function add_shortcode($tag, $func) {
			 global $shortcode_tags;

			 if ( is_callable($func) )
			  $shortcode_tags[$tag] = $func;
			}
			//$file = "http://ict.esterox.am/filestorage/home/Cohort2012/Course1/Semester1/Student Uploads/Artashes_Papikyan_statement.pdf";
			
			//echo '<iframe style="width:90%;height:100%" src="http://docs.google.com/gview?url='.$file.'&embedded=true#:0.page.20">';
		if(isset($_POST['search'])){
			$search_value = $_POST['search'];
			$attachmentsModel = new Attachments;
			if(!Yii::app()->user->isStudent()){
				$res = $attachmentsModel->topSearch($search_value,$this->user_id);
			}
			else
			{
				$res = $attachmentsModel->topSearch($search_value,$this->user_id,true);
				//var_dump($res);exit;
			}
			
			if(!empty($res)){
				
				for($i=0; $i<count($res); $i++){
					if($res[$i]['type'] == 1)
					{
						
						$data[$i]['type'] = end(explode(".", $res[$i]['path']));
						$data[$i]['name'] = end(explode("/", $res[$i]['path']));
						$data[$i]['path'] = $res[$i]['path'];
						$data[$i]['size'] = $res[$i]['size'];
						$data[$i]['date'] = $res[$i]['date'];
						if(strrpos($res[$i]['path'],'/') === false){
							$data[$i]['url'] = urlencode($data[$i]['name']);
						}else{
							//$name = substr($res[$i]['path'], 0,strrpos($res[$i]['path'], '/') );
							$data[$i]['url'] = urlencode($res[$i]['path']);//$name.'/'.urlencode($data[$i]['name']);
						}
						
						$data[$i]['user_id'] = $res[$i]['user_id'];
						
					}
					else
					{	
						$data[$i]['name'] = end(explode("/", $res[$i]['path']));
						$data[$i]['date'] = $res[$i]['date'];
						$data[$i]['type'] = "folder";
						$data[$i]['path'] = $res[$i]['path'];
						$data[$i]['size'] = "";
						$data[$i]['user_id'] = $res[$i]['user_id'];
						
					
					}
				}
				
			}
			else
			{
				$data = "";
			}
			echo json_encode($data);exit;
		}
		
		
			
			
			
			
		//}
		/*if(isset($_GET['operation'])) {
			$fs = new JstreeLib($user_uploads);
			try {
				$rslt = null;
				switch($_GET['operation']) {
					case 'get_node':
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						
						$rslt = $fs->lst($node, (isset($_GET['id']) && $_GET['id'] === '#'));
		
						break;
					case "get_content":
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
		
						$rslt = $fs->data($node);
						//var_dump($rslt);exit;
						break;
					case 'create_node':
						
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						if(!Yii::app()->user->isStudent() || prm_students($node)){
							$rslt = $fs->create($node, isset($_GET['text']) ? $_GET['text'] : '', (!isset($_GET['type']) || $_GET['type'] !== 'file'));
							$attachmentsModel = new Attachments;
							//$res_exist = $attachmentsModel->nodeExist($node);
		
							
								$attachmentsModel->path = $rslt['id'];
								$attachmentsModel->user_id = $this->user_id;
								$attachmentsModel->date = date('c',time());
								$attachmentsModel->type = 0;
								$attachmentsModel->save(false);
							//}
						}
						break;
					case 'rename_node':
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						if(!Yii::app()->user->isStudent() || prm_students($node)){
							$user_id = $attachmentsModel->select_user_id($node);
							//var_dump($user_id);exit;
							if(!Yii::app()->user->isStudent() || (isset($user_id['user_id']) && $user_id['user_id'] == Yii::app()->user->getId()))
							{
								$rslt = $fs->rename($node, isset($_GET['text']) ? $_GET['text'] : '');
								
								$find = $node;
								$replace = $rslt['id'];
								
								$model = Attachments::model()->findAll(array(
									'condition' => 'path LIKE :match AND user_id=:user_id',
									'params' => array(
										':match' => $find .'%',
										':user_id' => $this->user_id
									)
								));
								
								foreach($model as $key=>$item){
									$item->path = str_replace($find, $replace, $item->path);
									$item->save();
								}
							}else{
								$rslt = array('id'=>$node);
							}	
						}else{
							$rslt = array('id'=>$node);
						
						}
						
						break;
					case 'delete_node':
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						if(!Yii::app()->user->isStudent() || prm_students($node)){
							 
							$user_id = $attachmentsModel->select_user_id($node);
							if(!Yii::app()->user->isStudent() || (isset($user_id['user_id']) && $user_id['user_id'] == Yii::app()->user->getId()) ){
								$rslt = $fs->remove($node);
								$attachmentsModel = new Attachments;
								//var_dump($node);exit;
								$attachmentsModel->delete($node);
								/*$model = Attachments::model()->find(array(
									'condition' => 'path=:path AND user_id=:user_id',
									'params' => array(
										':path' => $node,
										':user_id' => $this->user_id
									)
								));*/
/*							}
						}
						break;
					case 'move_node':
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						$parn = isset($_GET['parent']) && $_GET['parent'] !== '#' ? $_GET['parent'] : '/';
						$rslt = $fs->move($node, $parn);
						break;
					case 'copy_node':
						$node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : '/';
						$parn = isset($_GET['parent']) && $_GET['parent'] !== '#' ? $_GET['parent'] : '/';
						$rslt = $fs->copy($node, $parn);
						break;
					default:
						throw new Exception('Unsupported operation: ' . $_GET['operation']);
						break;
				}
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($rslt);
			}
			catch (Exception $e) {
				header($_SERVER["SERVER_PROTOCOL"] . ' 500 Server Error');
				header('Status:  500 Server Error');
				echo $e->getMessage();
			}
			die();
		}*/
		if(!Yii::app()->user->isAdmin()){
			$attachmentsModel = new Attachments;
			$res = $attachmentsModel->all_upload_size($this->user_id);
			$this->uploadded_size = round($res['size']/1048576,3); //$this->userUploadded();
			$this->upload_size_of = $this->userAllowUpload();
		}
		$allowed_size = $this->userAllowUpload();
		//var_dump($user_uploaded_size);
		
		if(isset($_POST['file_id'])){
			
			$file_id = $_POST['file_id'];
			//var_dump($file_id);exit;
			if(!Yii::app()->user->isStudent() || prm_students($file_id)){
				
				$attachmentsModel = new Attachments;
				$path = $user_uploads.$file_id . DIRECTORY_SEPARATOR;
				$allowed_size = $this->userAllowUpload();
				
				$error = "";
					$total_size = $user_uploaded_size;
					$user_id = Yii::app()->user->getId();
					$rsss = $attachmentsModel->all_upload_size($user_id);
					$tottal_size = (int)($rsss['size']/1048576);

					for($i = 0; $i < count($_FILES['file']['name']); $i++){
						$f_name = str_replace("#","_",$_FILES['file']['name'][$i]);
						$tottal_size += (int)($_FILES['file']['size'][$i]/1048576);
						if(!$allowed_size || $tottal_size<=$allowed_size){
							if($file_id != "/"){
								$name = $attachmentsModel->exist_fileName_model($file_id.'/'.$f_name);
							}else{
								$name = $attachmentsModel->exist_fileName_model($f_name);
							}
							while(!empty($name)) {
								$n_name = $this->exist_fileName($f_name);
								if($file_id != "/"){
									$name = $attachmentsModel->exist_fileName_model($file_id.'/'.$n_name);
								}else{
									$name = $attachmentsModel->exist_fileName_model($n_name);
								}
								//$name = $attachmentsModel->exist_fileName_model($file_id.'/'.$n_name);
								$f_name = $n_name;
							}
							//var_dump($f_name);exit;
							$ext = explode('.', basename($_FILES['file']['name'][$i]));
							$tmpFilePath = $_FILES['file']['tmp_name'][$i];
							$size = (int)($_FILES['file']['size'][$i]);
							$total_size += (int)($_FILES['file']['size'][$i]/1048576);
							//if($total_size <= $allowed_size){
							//var_dump(strrpos($f_name,"("));exit;
								/*if(strrpos($f_name,"(") === false || strrpos($f_name,")") === false ){
									$file_name = $f_name;
									var_dump($file_name);exit;
								}else{
								
									$file_name = str_replace(")", "_", $f_name);
									$file_name = str_replace("(", "_", $file_name);
									var_dump("tr");exit;
									
								}*/
								$file_name = $f_name;
								$file_name_dir = urlencode($file_id .'/'.$file_name);
								if(move_uploaded_file($tmpFilePath, $path.$file_name)) {
									$up_data[] = array(
														'path' =>$file_id != "/"?$file_id .'/'. $file_name:$file_name,
														'user_id' => $this->user_id,
														'size' =>$size,
														'type' => 1,
														'date' => date('c',time())					
														);
									/*var_dump($tmpFilePath);var_dump($path.$file_name);
									$attachmentsModel->path = $file_id != "/"?$file_id .'/'. $file_name:$file_name;
									$attachmentsModel->user_id = $this->user_id;
									$attachmentsModel->size = $size;
									$attachmentsModel->type = 1;
									$attachmentsModel->date = date('c',time());
									$attachmentsModel->save();*/
								}
								$lst[] = array(
												'path' => $file_id != "/"?$file_id .'/'. $file_name:$file_name,
												'user_id' => $this->user_id,
												'size' => $size,
												'type' => 1,
												'date' => date('c',time()),
												'name' => $file_name,
												'url'=>$file_name_dir, //$file_id != "/"?$file_id .'/'. $file_name_dir:$file_name_dir,
												'ext' => end(explode('.', $_FILES['file']['name'][$i])),
												//'tottal_size'=>round($tottal_size,3)
											);
								
						}else{
							$lst = 'error';
							break;
						}
					}
					if(isset($up_data)){
						$attachmentsModel->save_data($up_data);
					}
					echo json_encode($lst);exit;
				//}

			}else{
				echo json_encode(false);
				exit;
			}
		}
		//var_dump($this->userAllowUpload());
		
		$this->render('mytree', array(
			'root' => $this->root,
			'user_uploads' => $user_uploads,
			'user_role' => $user_role,
			'base_folder_path' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage'
		));
		
		
		
    }
	
	private function str_coustm_replace($string,$search,$rep_str){
		//$bb = 'xxx/xxx/xxx';
		$text = "";
		$jj = "";
		for($i=0;$i<strlen($string);$i++){
			//$text = "";
			//$jj = "";
			$text .= $string[$i];
			if(isset($text1)){
				$jj .= $string[$i];
			}
			if($text == $search ){
				$text1 = $rep_str;
				
			}
			
		}
		return $text1.$jj;
	}
		
	
	
	private function exist_fileName($file_name){
			$pos = strrpos($file_name,'_');
			if($pos === false)
			{
			
				$num = 1;
				$ext = substr($file_name, strrpos($file_name, '.') + 1);
				$name = substr($file_name, 0,strrpos($file_name, '.') );
				$name = $name.'_'.$num.'.'.$ext;
				//var_dump($name.'=nnn');exit;
				return $name;
				
				
			}
			else
			{
				$ext = substr($file_name, strrpos($file_name, '.') + 1);
				//var_dump($ext);
				$name = substr($file_name, 0,strrpos($file_name, '.') );
				//var_dump($name);
				$n = substr($file_name, strrpos($file_name, '_') + 1);
				//var_dump($n);
				$nm = explode('.', $n);
				//var_dump($nm);
				$i= count($nm)-2;
				$num = $nm[$i];
				//echo $num;
				if(preg_match('/^[0-9]*$/', $num) )
				{
					$num = (int)$num+ 1;
					$full_n = substr($file_name, 0,strrpos($file_name, '_') ).'_'.$num.'.'.$ext;
					//var_dump($full_n."mmm");exit;
					return $full_n;
					
				}
				elseif(!preg_match('/^[0-9]*$/', $num) || $num == "" )
				{
					
					$n = 1;
					//$num = $num.'_'.$n;
					$full_n = $name.'_'.$n.'.'.$ext;
					//$this->exist_fileName($full_n);
					//var_dump($full_n."elseif");exit;
					return $full_n;
					
				}
			}
	
	}
	
    public function actionUploads() {
//        if(Yii::app()->request->isAjaxRequest){
//            
//        }
        echo $this->userUploadded();
        Yii::app()->end();
    }
   
    protected function userUploadded(){
        $attchmntsModel =  Attachments::model()->findAllByAttributes( array('user_id'=>Yii::app()->User->id));
        $uploadded_size = 0;
        if (!empty($attchmntsModel)) {
            foreach ($attchmntsModel as $attachment) {
                $uploadded_size += $attachment->size;
            }
                $uploadded_size = round($uploadded_size/1024000, 3);
        }
        return $uploadded_size;
    }
    
    protected function userAllowUpload(){
        if(Yii::app()->user->isDocent()){
            $upload_size_of = 1000;
        }elseif(Yii::app()->user->isStudent()){
            $upload_size_of = 500;
        }else{
            $upload_size_of = false;
        }
        return $upload_size_of;
    }
    
    protected function getElfinderAttributes(){
        
        if(Yii::app()->user->isAdmin()){
            return $attributes=array(
                array(
                    'pattern' => '!^/!',
                    'read' => true,
                    'write' => true,
                    'hidden' => false,
                    'locked' => false
                )
            );
        }
        
        $attchmntsModel = Attachments::model()->findAllByAttributes(array('user_id' => Yii::app()->User->id));
        
            $attributes = array(
                array(
                    'pattern' => '!^/!',
                    'read' => true,
                    'write' => false,
                    'hidden' => false,
                    'locked' => true
                )
            );
            
            
            
            if($this->userUploadded() < $this->userAllowUpload()){
                
                if(Yii::app()->user->isStudent()){
                    $attributes[] = array(
                            'pattern' => '/Student Uploads/',
                            'read' => true,
                            'write' => true,
                            'hidden' => false,
                            'locked' => true
                        );
                    $attributes[] = array(
                            'pattern' => '!/Student Uploads.*/([^\s]+(?=\.(.{2,5}))\.\2)$!',
                            'read' => false,
                            'write' => false,
                            'hidden' => true,
                            'locked' => true
                        );
//                    $attributes[] = array(
//                            'pattern' => '!/Student Uploads.*/([^\s]+(?=\.(jpg|gif|png))\.\2)$!',
//                            'read' => false,
//                            'write' => false,
//                            'hidden' => true,
//                            'locked' => true
//                        );
//                    $attributes[] = array(
//                            'pattern' => '!/Student Uploads.*/.*\.(bmp|jpeg|gif|png|jpg)$!',
//                            'read' => false,
//                            'write' => false,
//                            'hidden' => true,
//                            'locked' => true
//                        );
//                    $attributes[] = array(
//                            'pattern' => '!/Student Uploads.*/*.(jpg|pdf)$!',
//                            'read' => false,
//                            'write' => false,
//                            'hidden' => true,
//                            'locked' => true
//                        );
                }
                if(Yii::app()->user->isDocent()){
                    $attributes[] = array(
                        'pattern' => '/Cohort/',
                        'read' => true,
                        'write' => true,
                        'hidden' => false,
                        'locked' => true
                    );
                }
            }else{
                $attributes[] = array(
                        'pattern' => '/Cohort/',
                        'read' => true,
                        'write' => false,
                        'hidden' => false,
                        'locked' => true
                    );
            }
            
            
            
        if (!empty($attchmntsModel)) {
            foreach ($attchmntsModel as $attachment) {
                $attributes[] = array(
                    'pattern' => '!' . str_replace(Yii::getPathOfAlias('webroot') . '/filestorage', '', $attachment->path) . '!',
                    'read' => true,
                    'write' => true,
                    'hidden' => false,
                    'locked' => false
                );
            }
        }
       
        return $attributes;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
//      $this->layout = '//layouts/thememain';
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
//        $this->layout = '//layouts/thememain';
//        if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH)
//            throw new CHttpException(500, "This application requires that PHP was compiled with Blowfish support for crypt().");
		$this->layout = '//layouts/login';
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()){
				$this->redirect(Yii::app()->user->returnUrl);
			}
        }
        // display the login form
        $this->render('login', array('model' => $model, 'action' => 'login'));
    }
	private function foldersize($path) {
	  static $total_size = 0;
	  $files = scandir($path);

	  foreach($files as $t) {
		if (is_dir(rtrim($path, '/') . '/' . $t)) {
		  if ($t<>"." && $t<>"..") {
			  $size = $this->foldersize(rtrim($path, '/') . '/' . $t);

			  $total_size += $size;
		  }
		} else {
		  $size = filesize(rtrim($path, '/') . '/' . $t);
		  $total_size += $size;
		}
	  }
	  return round($total_size/1024000, 2);
	}

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
	private function prmDownload($file_name){
		$attachmentsModel = new Attachments;
		$user_id = Yii::app()->user->getId();
		
		$res = $attachmentsModel->prmDownloadFile($user_id,$file_name);
		//var_dump($res);exit;
		if(empty($res)){
			return false;
		}else{
			return true;
		}
	
	}
	
	/*private function removeDirectory($path) {
		$files = glob($path . '/*');
		foreach ($files as $file) {
			is_dir($file) ? removeDirectory($file) : unlink($file);
		}
		rmdir($path);
		return;
	}*/
	
		
	/*private	function recurse_copy($src,$dst) { 
		$dir = opendir($src); 
		@mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			
			if (( $file != '.' ) && ( $file != '..' ) ) { 
				if ( is_dir($src . '/' . $file) ) { 
				
					$this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
				} 
				else {
					$tmp = substr($src . '/' . $file,   $this->root_langth+1);
					
					if($this->prmDownload($tmp)){
						copy($src . '/' . $file,$dst . '/' . $file); 
					}
				} 
			} 
		} 
		closedir($dir); 
	} */
	
	public function actionDownload(){
		
		$folder = $_GET['id'];
		if($this->prmDownload($folder) || Yii::app()->user->isAdmin() || Yii::app()->user->isDocent()){
		//var_dump($folder);exit;
			$root_langth = strlen(Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR . 'home');
			$this->root_langth = $root_langth;
			$src_path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR . 'home' .DIRECTORY_SEPARATOR .$folder;
			
			//var_dump($folder);exit;
			
			//$src_path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR . $this->root . DIRECTORY_SEPARATOR . $folder;
			if(is_dir($src_path)){
				
				$tmp_dir = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR;
				//$this->recurse_copy($src_path,$tmp_dir.'tmp1');
				$zip_dir = $tmp_dir . 'tmp.zip';
				//$copy_dir = $tmp_dir.'tmp1';
				
				
				$zip = Yii::app()->zip;
				$zip->makeZip($src_path, $zip_dir); // make an ZIP archive
				//var_dump($tmp_dir.'tmp.zip', $zip_dir);exit;
				if(file_exists($zip_dir)) {
					//var_dump($tmp_dir.'tmp.zip', $zip_dir);exit;
					$filename = end(explode('/', $folder));
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename='.$filename.'.zip');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					header('Content-Length: ' . filesize($zip_dir));
					if (ob_get_contents()){
					ob_clean();
					}
					flush();
					
					if (readfile($zip_dir)) 
					{
						unlink($zip_dir);
						
						/*$this->removeDirectory($copy_dir);
						unlink($copy_dir);*/
					}
				}
			} else {
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header("Content-Disposition: attachment; filename=\"".basename($src_path)."\"");
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($src_path));
				if (ob_get_contents()){
				ob_clean();
				}
				flush();
				readfile($src_path);
			}
		}else{
			throw new CHttpException(404,'The specified post cannot be found.');
	
		}	
	}
}