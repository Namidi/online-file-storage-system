<?php

class ForgotPasswordController extends Controller {

    /**
     * Declares class-based actions.
     */
	
    public function accessRules() {
        return array(
            array('deny',
                'actions' => array('index', 'confirm'),
                'expression' => '', //'Yii::app()->user->isGuest'
            ),
        );
    }
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
		$data = array();
		if(isset($_POST['forgot'])){
			$email = $_POST['email'];
			$is_exist = User::model()->get_user($email);
			if(!$is_exist){
				$data['email_error'] = true;
			}else{
				$token = User::model()->add_fp_token($email);
				$headers="From: <{$email}>\r\n".
                    "Reply-To: {$email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/html; charset=UTF-8";
					$message = "<div>Beste Gebruiker,</div><br><a href='".Yii::app()->params['domain']."index.php/forgotPassword/reset?token=".$token."'>Hierbij de link voor uw UNASAT OFSS password reset.</a><br><br><div>Mvg,</div><div>Het UNASAT Team</div>";
               mail($email,"Password reset link UNASAT OFSS",$message,$headers);
			   $data['success'] = true;
			}
		}
		$this->render('index', $data);
	}
	public function actionReset(){
		$data = array();
		$token = isset($_GET['token'])?$_GET['token']:false;
		if($token){
			$is_exist = User::model()->is_token_exist($token);
			if($is_exist){
				if(isset($_POST['reset'])){
					$password = $_POST['password'];
					$re_password = $_POST['re_password'];
					if(empty($password)){
						$data['empty_password'] = true;
					}elseif($password != $re_password){
						$data['not_equal_password'] = true;
					}else{
							
						User::model()->change_password($is_exist['email'], $password);
						User::model()->delete_token($token);
						$data['passwords_changed'] = true;
						//$this->redirect(Yii::app()->homeUrl);
					}
				}
			}else{
				echo "wrong Action";exit;
			}
		}
		$this->render('reset', $data);
	}
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}