<?php
class JstreeLib
{
	protected $base = null;

	protected function real($path) {
		$temp = realpath($path);
		if(!$temp) { throw new Exception('Path does not exist: ' . $path); }
		if($this->base && strlen($this->base)) {
			if(strpos($temp, $this->base) !== 0) { throw new Exception('Path is not inside base ('.$this->base.'): ' . $temp); }
		}
		return $temp;
	}
	
	protected function path($id) {
		$id = str_replace('/', DIRECTORY_SEPARATOR, $id);
		$id = trim($id, DIRECTORY_SEPARATOR);
		$id = $this->real($this->base . DIRECTORY_SEPARATOR . $id);
		return $id;
	}
	
	protected function id($path) {
		$path = $this->real($path);
		$path = substr($path, strlen($this->base));
		$path = str_replace(DIRECTORY_SEPARATOR, '/', $path);
		$path = trim($path, '/');
		return strlen($path) ? $path : '/';
	}
	 
	private  function prm_students($node){
		if(isset($node)){
			$arr = explode('/', $node);
			for($i = 0; $i<count($arr); $i++){
				if($arr[$i] == "Student Uploads"){
					return TRUE;
					break;
				}
				
			}
		}
	}

	public function __construct($base) {
		$this->base = $this->real($base);
		if(!$this->base) { throw new Exception('Base directory does not exist'); }
	}
	
	public function lst($id, $with_root = false) {
		$role = !Yii::app()->user->isStudent()?true:false;
		$node = $id;
		$dir = $this->path($id);
		$lst = @scandir($dir);
		if(!$lst) { throw new Exception('Could not list path: ' . $dir); }
		$res = array();
		/*foreach($lst as $item) {
			
			if($item == '.' || $item == '..' || $item === null) { continue; }
			$tmp = preg_match('([^ a-z?-?-_0-9.]+)ui', $item);
			if($tmp === false || $tmp === 1) { continue; }
			if(is_dir($dir . DIRECTORY_SEPARATOR . $item)) {
				$res[] = array('text' => $item, 'children' => true,  'id' => $this->id($dir . DIRECTORY_SEPARATOR . $item), 'icon' => 'folder');
			}
			else {
				$res[] = array('text' => $item, 'children' => false, 'id' => $this->id($dir . DIRECTORY_SEPARATOR . $item), 'type' => 'file', 'icon' => 'file file-'.substr($item, strrpos($item,'.') + 1));
			}
		}*/
		
		$pathConcat = "";
			if($id == "/"){
				foreach($lst as $k => $v){
					if($v != '.' && $v != '..'){
						$pathConcat .= "'".$v."'".",";
					}
				}	
			}
			else
			{
				foreach($lst as $k => $v){
					if($v != '.' && $v != '..' && $v != ""){
						$pathConcat .= "'".$id.'/'.$v."'".",";
					}
				}	
					
			}
			
			if($pathConcat != ""){
				$user_id = Yii::app()->user->getId();
				$ress = Attachments::model()->selectStdId($user_id,rtrim($pathConcat,','),$role);
				
				foreach($ress as $k => $v){
					array_unshift($v, ".", "..");
					$name = end(explode('/', $v['path']));
					if($v['type'] == 0){
						$res[] = array('text' => $name, 'children' => true,  'id' => $this->id($dir . DIRECTORY_SEPARATOR . $name), 'icon' => 'folder');
					}
					else
					{
						$res[] = array('text' => $name, 'children' => false, 'id' => $this->id($dir . DIRECTORY_SEPARATOR . $name), 'type' => 'file', 'icon' => 'file file-'.substr($name, strrpos($name,'.') + 1));
					
					}
				}
			}
			//var_dump($res);exit;
		if($with_root && $this->id($dir) === '/') {
			$dir_contain = array();
			$dir_contain_ = scandir($dir);
			$pathConcat = "";
			if($id == ""){
				foreach($dir_contain_ as $k => $v){
					if($v != '.' && $v != '..'){
						$pathConcat .= "'".$v."'".",";
					}
				}	
			}
			else
			{
				foreach($dir_contain_ as $k => $v){
					if($v != '.' && $v != '..' && $v != ""){
						$pathConcat .= "'".$id.'/'.$v."'".",";
					}
				}	
					
			}
			if($pathConcat != ""){
				$user_id = Yii::app()->user->getId();
				$res = Attachments::model()->selectStdId($user_id,rtrim($pathConcat,','),$role);
				foreach($res as $k => $v){
					$name = end(explode('/', $v['path']));
					$dir_contain[] = array(
										'type' => $v['type'] ==0?'folder':'file',
										'name' => $name,
										'file_id' => $v['path'],
										'size' => $v['type'] ==0?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $name),
										'date' => $v['date']
									);
			
				}
			}
			
			/*
			foreach($dir_contain_ as $k => $v){
				if($v != '.' && $v != '..'){
					//if(is_dir($dir . DIRECTORY_SEPARATOR . $v) && Yii::app()->user->isStudent() || $this->prm_students($node)){
						$dir_contain[] = array(
							'type' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'folder':'file',
							'name' => $v,
							'file_id' => $id . DIRECTORY_SEPARATOR . $v,
							'size' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $v),
							'date' => date('d-m-Y', filectime($dir . DIRECTORY_SEPARATOR . $v))
						);
					//}
					/*elseif(!Yii::app()->user->isStudent())
					{
						$dir_contain[] = array(
							'type' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'folder':'file',
							'name' => $v,
							'file_id' => $id . DIRECTORY_SEPARATOR . $v,
							'size' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $v),
							'date' => date('d-m-Y', filectime($dir . DIRECTORY_SEPARATOR . $v))
						);
					
					}
				}
			}*/

			$dir_contain = array_diff($dir_contain, array('.', '..'));
			$res = array(array('text' => basename($this->base), 'children' => $res, 'id' => '/', 'icon'=>'folder', 'dir_contain' => $dir_contain, 'state' => array('opened' => true, 'disabled' => true)));
		}
		return $res;
	}
	
	
	
	public function data($id) {
		$role = !Yii::app()->user->isStudent()?true:false;
		$node = $id;
		if(strpos($id, ":")) {
			$id = array_map(array($this, 'id'), explode(':', $id));
			return array('type'=>'multiple', 'content'=> 'Multiple selected: ' . implode(' ', $id));
		}
		$dir = $this->path($id);
		
		if(is_dir($dir) ) {
			$filename = explode('/', $dir);
			$filename = end($filename);
			$dir_contain = array();
			
			$dir_contain_ = scandir($dir);
			$pathConcat = "";
			if($id == ""){
				foreach($dir_contain_ as $k => $v){
					if($v != '.' && $v != '..'){
						$pathConcat .= "'".$v."'".",";
					}
				}	
			}
			else
			{
				foreach($dir_contain_ as $k => $v){
					if($v != '.' && $v != '..' && $v != ""){
						$pathConcat .= "'".$id.'/'.$v."'".",";
					}
				}	
					
			}
			if($pathConcat != ""){
				$user_id = Yii::app()->user->getId();
				$res = Attachments::model()->selectStdId($user_id,rtrim($pathConcat,','),$role);
				foreach($res as $k => $v){
					$name = end(explode('/', $v['path']));
					$dir_contain[] = array(
										'type' => $v['type'] ==0?'folder':'file',
										'name' => $name,
										'file_id' => $v['path'],
										'size' => $v['type'] ==0?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $name),
										'date' => $v['date']
									);
			
				}
			}
			
			
			/*foreach($dir_contain_ as $k => $v){
				if($v != '.' && $v != '..'){
					if(Yii::app()->user->isStudent() && $this->prm_students($node)){
						
					//trim($id . DIRECTORY_SEPARATOR . $v, '/')
						$dir_contain[] = array(
							'type' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'folder':'file',
							'name' => $v,
							'file_id' => trim($id . DIRECTORY_SEPARATOR . $v, '/'),
							'size' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $v),
							'date' => date('d-m-Y', filectime($dir . DIRECTORY_SEPARATOR . $v))
						);
					}
					else //if(!Yii::app()->user->isStudent())
					{
						$dir_contain[] = array(
						'type' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'folder':'file',
						'name' => $v,
						'file_id' => trim($id . DIRECTORY_SEPARATOR . $v, '/'),
						'size' => is_dir($dir . DIRECTORY_SEPARATOR . $v)?'':SiteLib::getFileSize($dir . DIRECTORY_SEPARATOR . $v),
						'date' => date('d-m-Y', filectime($dir . DIRECTORY_SEPARATOR . $v))
						);
					}
				}
			}*/
			$dir_contain = array_diff($dir_contain, array('.', '..'));
			return array('type'=>'folder', 'content'=> $id,'dir_contain' => $dir_contain);
		}
		if(is_file($dir)  ) {
			$ext = strpos($dir, '.') !== FALSE ? substr($dir, strrpos($dir, '.') + 1) : '';
			$dat = array('type' => $ext, 'content' => '');
			switch($ext) {
				case 'txt':
				case 'text':
				case 'md':
				case 'js':
				case 'json':
				case 'css':
				case 'html':
				case 'htm':
				case 'xml':
				case 'c':
				case 'cpp':
				case 'h':
				case 'sql':
				case 'log':
				case 'py':
				case 'rb':
				case 'htaccess':
				case 'php':
					$dat['content'] = file_get_contents($dir);
					break;
				case 'jpg':
				case 'jpeg':
				case 'gif':
				case 'png':
				case 'bmp':
					$dat['content'] = 'data:'.finfo_file(finfo_open(FILEINFO_MIME_TYPE), $dir).';base64,'.base64_encode(file_get_contents($dir));
					break;
				default:
					$dat['content'] = 'File not recognized: '.$this->id($dir);
					break;
			}
			$dat['file_type'] = 'file';
			return $dat;
		}
		throw new Exception('Not a valid selection: ' . $dir);
	}
	
	public function create($id, $name, $mkdir = false) {
			$dir = $this->path($id);
			if(preg_match('([^ a-z?-?-_0-9.]+)ui', $name) || !strlen($name)) {
				throw new Exception('Invalid name: ' . $name);
			}
			if($mkdir) {
				mkdir($dir . DIRECTORY_SEPARATOR . $name);
			}
			else {
				file_put_contents($dir . DIRECTORY_SEPARATOR . $name, '');
			}
			return array('id' => $this->id($dir . DIRECTORY_SEPARATOR . $name));
		
	}
	
	public function rename($id, $name) {
			$dir = $this->path($id);
			if($dir === $this->base) {
				throw new Exception('Cannot rename root');
			}
			/*if(preg_match('([^ a-z?-?-_0-9.]+)ui', $name) || !strlen($name)) {
				throw new Exception('Invalid name: ' . $name);
			}*/
			//str_replace("#","_",$name);
			$new = explode(DIRECTORY_SEPARATOR, $dir);
			array_pop($new);
			array_push($new, $name);
			$new = implode(DIRECTORY_SEPARATOR, $new);
			if($dir !== $new) {
				if(is_file($new) || is_dir($new)) { throw new Exception('Path already exists: ' . $new); }
				rename($dir, $new);
			}
			return array('id' => $this->id($new));
		
	}
	
	public function remove($id) {
		$dir = $this->path($id);
		if($dir === $this->base) {
			throw new Exception('Cannot remove root');
		}
		if(is_dir($dir)) {
			foreach(array_diff(scandir($dir), array(".", "..")) as $f) {
				$this->remove($this->id($dir . DIRECTORY_SEPARATOR . $f));
			}
			rmdir($dir);
		}
		if(is_file($dir)) {
			unlink($dir);
		}
		return array('status' => 'OK');
	}
	
	public function move($id, $par) {
		$dir = $this->path($id);
		$par = $this->path($par);
		$new = explode(DIRECTORY_SEPARATOR, $dir);
		$new = array_pop($new);
		$new = $par . DIRECTORY_SEPARATOR . $new;
		rename($dir, $new);
		return array('id' => $this->id($new));
	}
	
	public function copy($id, $par) {
		$dir = $this->path($id);
		$par = $this->path($par);
		$new = explode(DIRECTORY_SEPARATOR, $dir);
		$new = array_pop($new);
		$new = $par . DIRECTORY_SEPARATOR . $new;
		if(is_file($new) || is_dir($new)) { throw new Exception('Path already exists: ' . $new); }

		if(is_dir($dir)) {
			mkdir($new);
			foreach(array_diff(scandir($dir), array(".", "..")) as $f) {
				$this->copy($this->id($dir . DIRECTORY_SEPARATOR . $f), $this->id($new));
			}
		}
		if(is_file($dir)) {
			copy($dir, $new);
		}
		return array('id' => $this->id($new));
	}
}
?>