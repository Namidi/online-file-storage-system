<?php

class SiteLib
{
	public static function getFileSize($path){
		$size = filesize($path);
		$unit = 'B';
		
		if($size > 1024){
			$size = $size / 1024;
			$unit = 'KB';
			if($size > 1000) {
				$size = $size / 1000;
				$unit = 'MB';
				if($size > 1000) {
					$size = $size / 1000;
					$unit = 'GB';
				}
			}
		}
		
		return round($size, 2) .' '. $unit;
	}
}
