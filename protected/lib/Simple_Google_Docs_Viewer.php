<?php
/**
 * Shortcode handler wrapper
 * 
 * @author Maor Chasen <info@illuminea.com>
 */
class Simple_Google_Docs_Viewer {
	/**
	 * Get things moving
	 *
	 * @uses add_shortcode() for initializing the shortcode
	 */
	function __construct() {
		add_shortcode( 'gviewer', array( $this, 'the_shortcode' ) );
	}
	/**
	 * The actual shortcode.
	 *
	 * @since 1.0
	 * @param  array $atts Shortcode attributes
	 * @param  string $content Not used at this moment
	 * @return mixed The embed HTML on success, null on failure
	 */
	function the_shortcode( $atts, $content = '' ) {
		extract( apply_filters( 'simple_gviewer_atts', shortcode_atts( array(
			'file' => '',
			'width' => 600,
			'height' => 700,
			'language' => 'en'
		), $atts ) ) );
		if ( '' != ( $file = apply_filters( 'simple_gviewer_file_url', $file ) ) ) {
			$embed_format = '<iframe src="http://docs.google.com/viewer?url=%1$s&embedded=true&hl=%2$s" width="%3$d" height="%4$d" style="border: none;"></iframe>';
			
			return sprintf( $embed_format, 
				urlencode( esc_url( $file, array( 'http', 'https' ) ) ),
				esc_attr( $language ),
				absint( $width ),
				absint( $height )
			);
		}
		// No file specified, bail.
		return;
	}
}
