<div id="container" role="main">
	<div class="left_side">
		<!--<div class="infont col-md-3 col-sm-4"><a href="#"><i class="fa fa-times-circle"></i></a></div>-->
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-bottom:10px;"></div>
		<form id="files_form" method="post" action="" enctype="multipart/form-data">
			<label style="width:276px;margin-left:20px;" title=" Upload Files " for="inputImage" class="btn btn-primary">
				<input type="file"  name="file[]" id="inputImage" class="hide" multiple="multiple">
				Upload Files
			</label>
			<input type="hidden" id="file_id"  name="file_id" value="" />
		</form>
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-top:10px;"></div>
		<div id="tree"></div>
	</div>
	<div id="data">
		<div class="content code" style="display:none;"><textarea id="code" readonly="readonly"></textarea></div>
		<div class="content folder" style="display:none;"></div>
		<div class="content image" style="display:none; position:relative;"><img src="" alt="" style="display:block; position:absolute; left:50%; top:50%; padding:0; max-height:90%; max-width:90%;" /></div>
		<div class="default" style="">
			<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />
		</div>
	</div>
</div>

<script src="<?php echo Yii::app()->baseUrl; ?>/jstree/js/jstree.js"></script>
<script>
window.base_folder_path = '<?php echo Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR . $root . DIRECTORY_SEPARATOR; ?>';
window.loader = '<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />';
window.user_uploads = "<?php echo $user_uploads; ?>";
var opend_folder;

$(function () {
	
	$(window).resize(function () {
			$('.files_datatable').css('width', '100%');
		//var h = Math.max($(window).height() - 0, 420);
		//$('#container, #data, #tree, #data .content').height(h).filter('.default');//.css('lineHeight', h + 'px');
	}).resize();
	
	$(document).on('click','.open_dir', function(){
		var file_id = $(this).attr('file_id');
		if(file_id == '/'){
			$('#tree').jstree("refresh", true, true);
			$('#tree').jstree("refresh");
		}	
		var res = file_id.split("/");
		$("#tree").jstree('open_node','/');
		var str = '';
		for(var i = 0; i< res.length; i++){
			str += (i == 0?'':'/') + res[i];
			$("#tree").jstree('open_node', str);
		}
		$("[id='"+file_id+"_anchor']").trigger('dblclick');
		$("[id='"+file_id+"_anchor']").trigger('click');
		
		return false;
	});
	
	$(document).on('click', '.remove', function(){
		var file_id = $(this).attr('file_id');
		$("#tree").jstree("delete_node", file_id);
		$('#tree').jstree("refresh");
	});
	
	$('#tree').on("select_node.jstree", function (e, data) {
	//	alert("node_id: " + data.node.id);
		$("[id='"+data.node.id+"_anchor']").trigger('dblclick');
	});
	
	/*
	$("#tree").bind("select_node.jstree", function (event, data) {
		if(typeof(opend_folder.node) !== 'undefined'){
			parents = opend_folder.node.parents;
			for(var i in parents) {
				$("#tree").jstree('open_node', $('#'+parents[i]));
			//	$("#tree").jstree("select_node", selector).trigger("select_node.jstree");
			}
		}
	}); 
	*/
	$(document).on('click', '.view', function(e, data){
		///e.preventDefault();
		//var file_id = $(this).attr('file_id');
	//	console.log(opend_folder);
		
	//	$("#tree").jstree('open_node', $('#'+opend_folder.node.id));
		
	//	parent_node = data.node.parents[0];
	//	alert(file_id)
	/*
		$(parent_node).find('i.jstree-icon.jstree-themeicon').first()
			.removeClass('glyphicon-folder-open').addClass('glyphicon-folder-close');
	*/	
	//$("[id='"+file_id+"_anchor']").trigger('click');
	//	$("#tree").jstree("dblclick.jstree", file_id);
	//	$('#tree').jstree("refresh");
	//	$(this).attr('id', file_id);
	});
	$(document).on('click', '.create_new_folder', function(){
		var file_id = $(this).attr('file_id');
		$("#tree").jstree("create_node", file_id);
		//$('#tree').jstree("refresh");
		//setTimeout(function(){ 
			var res = file_id.split("/");
			$("#tree").jstree('open_node','/');
			var str = '';
			for(var i = 0; i< res.length; i++){
				str += (i == 0?'':'/') + res[i];
				$("#tree").jstree('open_node', str);
			}
			$("[id='"+file_id+"_anchor']").trigger('dblclick');
			$("[id='"+file_id+"_anchor']").trigger('click');
		//}, 1000);
		
		
		return false;		
	});
	$(document).on('click', '.jstree-icon', function(e, data){
		id = opend_folder.node.id;

	//	$('#tree').bind()
	});
	//$("#tree").jstree("close_all");
	$("#tree").bind("loaded.jstree", function(event, data) { 
		//data.instance.close_all();
    });
	$(document).on('change', '#inputImage', function(){
		if ($('#inputImage').val() == '') {
            return false;
        }
		if(opend_folder.selected.length){
			$('#file_id').val(opend_folder.selected);
			
		}
		$('#files_form').ajaxForm({
            target: '.images_upload_error',
            success: function() {

            },
            beforeSend: function() {

            },
            uploadProgress: function(event, position, total, percentComplete){
		        if(percentComplete == 100) {
								
					var file_id = opend_folder.selected.length?opend_folder.selected[0]:false;
					if(!file_id){
						$('#tree').jstree("refresh");
						return false;
					}
					$('#tree').jstree("refresh");
                    var res = file_id.split("/");
					$("#tree").jstree('open_node','/');
					var str = '';
					for(var i = 0; i< res.length; i++){
						str += (i == 0?'':'/') + res[i];
						$("#tree").jstree('open_node', str);
					}
					$("[id='"+file_id+"_anchor']").trigger('dblclick');
					$("[id='"+file_id+"_anchor']").trigger('click');
					
                }
            }
        }).submit();
	});
	A();
	function A(){
	localStorage.setItem("jstree", '{"state":{"core":{"open":["/"],"scroll":{"left":0,"top":0},"selected":[]}},"ttl":false,"sec":1426839152223}');
	$('#tree')
		.jstree({
			'core' : {
				'data' : {
					'url' : '?operation=get_node',
					'data' : function (node) {
						
						return { 'id' : node.id };
					}
				},
				'check_callback' : function(o, n, p, i, m) {
					if(m && m.dnd && m.pos !== 'i') { return false; }
					if(o === "move_node" || o === "copy_node") {
						if(this.get_node(n).parent === this.get_node(p).id) { return false; }
					}
					return true;
				},
				'themes' : {
					'responsive' : false,
					'variant' : 'small',
					'stripes' : true
				}
			},
			'search': {
                'show_only_matches': true,
                'fuzzy': false
            },
            'plugins': ['search', 'sort'],
			'sort' : function(a, b) {
				return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
			},
			'contextmenu' : {
				'items' : function(node) {
					var tmp = $.jstree.defaults.contextmenu.items();
					delete tmp.create.action;
					tmp.create.label = "New";
					tmp.create.submenu = {
						"create_folder" : {
							"separator_after"	: true,
							"label"				: "Folder",
							"action"			: function (data) {
								var inst = $.jstree.reference(data.reference),
									obj = inst.get_node(data.reference);
								inst.create_node(obj, { type : "default" }, "last", function (new_node) {
									setTimeout(function () { inst.edit(new_node); },0);
								});
							}
						},
						"create_file" : {
							"label"				: "File",
							"action"			: function (data) {
								var inst = $.jstree.reference(data.reference),
									obj = inst.get_node(data.reference);
								inst.create_node(obj, { type : "file" }, "last", function (new_node) {
									setTimeout(function () { inst.edit(new_node); },0);
								});
							}
						}
					};
					if(this.get_type(node) === "file") {
						delete tmp.create;
					}
					return tmp;
				}
			},
			"multiple": false,
			'types' : {
				'default' : { 'icon' : 'folder' },
				'file' : { 'valid_children' : [], 'icon' : 'file' }
			},
			'unique' : {
				'duplicate' : function (name, counter) {
					return name + ' ' + counter;
				}
			},
			'plugins' : ['state','dnd','sort','types','contextmenu','unique']
		})
		.on('delete_node.jstree', function (e, data) {
			$.get('?operation=delete_node', { 'id' : data.node.id })
				.fail(function () {
					data.instance.refresh();
				});
			parent_node = data.node.parents[0];
			$("[id='"+parent_node+"_anchor']").trigger('click');
		})
		.on('create_node.jstree', function (e, data) {
			$.get('?operation=create_node', { 'type' : data.node.type, 'id' : data.node.parent, 'text' : data.node.text })
				.done(function (d) {
					data.instance.set_id(data.node, d.id);
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('rename_node.jstree', function (e, data) {
			$.get('?operation=rename_node', { 'id' : data.node.id, 'text' : data.text })
				.done(function (d) {
					data.instance.set_id(data.node, d.id);
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('move_node.jstree', function (e, data) {
			$.get('?operation=move_node', { 'id' : data.node.id, 'parent' : data.parent })
				.done(function (d) {
					//data.instance.load_node(data.parent);
					data.instance.refresh();
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('copy_node.jstree', function (e, data) {
			$.get('?operation=copy_node', { 'id' : data.original.id, 'parent' : data.parent })
				.done(function (d) {
					//data.instance.load_node(data.parent);
					data.instance.refresh();
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
//		.on("click", "a", function (e) {
		//	$('#tree').jstree("refresh");
//		})
		.on('changed.jstree', function (e, data) {
			opend_folder = data;
		//	console.log(data.node.id);
			if(true || data && data.selected && data.selected.length) {
				if(data.selected.length > 1){
					//data.selected = [data.selected[1]];
				}
				$.get('?operation=get_content&id=' + data.selected.join(':'), function (d) {
					
					if(d && typeof d.type !== 'undefined') {
						$('#data .content').hide();
						switch(d.type) {
							case 'text':
							case 'txt':
							case 'md':
							case 'htaccess':
							case 'log':
							case 'sql':
							case 'php':
							case 'js':
							case 'json':
							case 'css':
							case 'html':
								$('#data .code').show();
								$('#code').val(d.content);
								break;
							case 'png':
							case 'jpg':
							case 'jpeg':
							case 'bmp':
							case 'gif':
								$('#data .image img').one('load', function () { $(this).css({'marginTop':'-' + $(this).height()/2 + 'px','marginLeft':'-' + $(this).width()/2 + 'px'}); }).attr('src',d.content);
								$('#data .image').show();
								break;
							default:
								var dir_contain = d.dir_contain;
								var file_icons = {
									'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
									'png':'<a><i class="fa fa-file-image-o"></i></a>',
									'gif':'<a><i class="fa fa-file-image-o"></i></a>',
									'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
									'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
									'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
									'doc': '<a><i class="fa fa-file-word-o"></i></a>',
									'docx': '<a><i class="fa fa-file-word-o"></i></a>',
									'txt': '<a><i class="fa fa-file-text"></i></a>',
									'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
									'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
									'default': '<a><i class="fa fa-file-o"></i></a>',
									
								};
								var html = '<div class="row icons-box">';
									html += '<div class="infont col-lg-12" style="width:98%">';
										html += '<div class="ibox float-e-margins">';
										html += '<div class="ibox-content table-responsive">';
										html += '<ol class="breadcrumb">';
											html += '<li><a file_id="/" class="open_dir">Home</a></li>';
											var brcs = (d.content).split("/");
											var str = ''; 
											if(brcs.length){
												for(var i = 0; i < brcs.length; i++){
													if(i == brcs.length - 1){
														html += '<li class="active"style="color:#1ab394"><strong>' + brcs[i] + '</strong></li>';
													}else{
														str += (i == 0?'':'/') + brcs[i];
														html += '<li><a class="open_dir" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
													}
												}
											}
										html += '</ol>';
										html += '<a file_id = "' + d.content + '" class="create_new_folder" href="">+Create New Folder</a>';
								    html += '<table class="table table-hover files_datatable">';
									html += '<thead>';
										html += '<tr>';
											html += '<th>Type</th>';
											html += '<th>Name</th>';
											html += '<th>Download/View</th>';
											html += '<th>Remove</th>';
											html += '<th>Size</th>';
											html += '<th>Date</th>';
										html += '</tr>';
									html += '</thead>';
									html += '<tbody>';
										for(var i in dir_contain){
											var root = '';
											if(i == 0){
												root = 'root';
											}
											html += '<tr class="'+root+'">';
												if(dir_contain[i].type == 'folder'){
													html += '<td><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
													html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
													html +=	'<td><a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="download_item fa fa-file-zip-o" file_id="'+dir_contain[i].file_id+'" ></i></a></td>';
													html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													html += '<td></td>';
												}else{
													//file_icons
													var ext = (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1);
													var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
													
													html += '<td><span style="display:none;">'+ (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1)+'</span>' + file_icon + '</td>';
													html += '<td>'+dir_contain[i].name+'</td>';
													html +=	'<td>';
														html +=	'<a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="fa fa-save download_item"></i></a>';
														html +=	'<a data-gallery="" target="_blank" href="'+user_uploads + dir_contain[i].file_id+'" class="view" file_id="'+dir_contain[i].file_id+'"><i class="view_item fa fa-eye"></i></a>';
													html +=	'</td>';
													html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													html += '<td>'+dir_contain[i].size+'</td>';
												}
												html += '<td>'+dir_contain[i].date+'</td>'; 
											html += '</tr>';
										}
									html += '</tbody>';
									/*html += '<tfoot>';
										html += '<tr>';
											html += '<th>Type</th>';
											html += '<th>Name</th>';
											html += '<th>Download/View</th>';
											html += '<th>Remove</th>';
											html += '<th>Size</th>';
											html += '<th>Date</th>';
										html += '</tr>';
									html += '</tfoot>';*/
								html += '</table>';
								html += '</div></div></div></div>';
								if(d.file_type != 'file'){
									$('#data .default').html(html).show();
									
										
									$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false
										//bFilter:false
										
									});
								}
								break;
						}
					}
				});
			}
			else {
				//$('#data .content').hide();
				//$('#data .default').html('').show();
			}
		});
	}
});
</script>
