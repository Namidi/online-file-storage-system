<?php

class JsTreeWidget extends CWidget
{
	public $root;
	public $base_folder_path;
	public function run()
    {
        $this->render('jstree', array(
			'root' => $this->root, 
			'base_folder_path' => $this->base_folder_path
		));
    }
}

