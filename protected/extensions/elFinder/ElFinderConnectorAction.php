<?php
/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
class ElFinderConnectorAction extends CAction
{
    /**
     * https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options
     * @var array
     */
    public $settings = array();

    public function run()
    {
        error_reporting(0); // Set E_ALL for debuging

        include_once dirname(__FILE__) . '/php/elFinderConnector.class.php';
        include_once dirname(__FILE__) . '/php/elFinder.class.php';
        include_once dirname(__FILE__) . '/php/elFinderVolumeDriver.class.php';
        //include_once dirname(__FILE__) . '/php/elFinderVolumeLocalFileSystem.class.php';
        // Required for MySQL storage connector
        // include_once dirname(__FILE__).'/php/elFinderVolumeMySQL.class.php';
        // Required for FTP connector support
        // include_once dirname(__FILE__).'/php/elFinderVolumeFTP.class.php';


        /**
         * Simple function to demonstrate how to control file access using "accessControl" callback.
         * This method will disable accessing files/folders starting from  '.' (dot)
         *
         * @param  string  $attr  attribute name (read|write|locked|hidden)
         * @param  string  $path  file path relative to volume root directory started with directory separator
         * @param $data
         * @param $volume
         * @return bool|null
         */
        function access($attr, $path, $data, $volume)
        {
            return strpos(basename($path), '.') === 0 // if file/folder begins with '.' (dot)
                ? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
                : null; // else elFinder decide it itself
        }
        
        function customValidName($name) {
            if(!strpos($name, '/^\w[\w\s\.\%\-]*$/u') && !strpos($name, ')') && !strpos($name, '(')){
                return true;
            }
        }
//        die('here');
        function customCallback($cmd, $result, $args, $elfinder) {
           
            
//                    file_put_contents ('response.txt', print_r(array($cmd, $result), true));
//                    file_put_contents ( 'response.txt', $cmd);
//            file_put_contents ( '../response.txt', print_r($result, true));
//            file_put_contents ( './response.txt', print_r(array($result, $args,$elfinder), true));
//            foreach((array)$elfinder as $value=>$key){
//             $attchModel =  Attachments::model()->findByAttributes( array('path'=>$elfinder->realpath($result['added'][0]['hash'])));
            
                        
            if($cmd == 'upload' || $cmd == 'mkdir'){
                 if(Yii::app()->user->role == 1) return true;
                $attchModel =  new Attachments;
                foreach($result['added'] as $attachment){                        
                    $attchModel->id=false;
                    $attchModel->isNewRecord=true;
                    $attchModel->path = $elfinder->realpath($attachment['hash']);
                    $attchModel->size = $attachment['size'];
                    $attchModel->save();
                }
            }
            
//                        file_put_contents ( 'response.txt', 'ggggg' );
            if($cmd == 'rm'){
//                        file_put_contents ( 'response.txt', print_r(array($result, $cmd, $result['removed'] ), true));
                
                foreach($result['removed'] as $attachment){
                    $attchModel = Attachments::model()->findByAttributes(array(
                                'path' => $attachment['realpath']
                            )
                    );
                    if($attchModel){
                        $attchModel->delete();
                    }
                }
            }
            
            
//            file_put_contents ( './response.txt', print_r($elfinder->realpath($result['added'][0]['hash']), true));
//            $result
            return true;
        }
        
        $opts = $this->settings;
        foreach ($opts['roots'] as &$r) {
            switch ($r['driver']) {
                case 'LocalFileSystem':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeLocalFileSystem.class.php';
                    break;
                case 'MySQL':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeMySQL.class.php';
                    break;
                case 'FTP':
                    include_once dirname(__FILE__) . '/php/elFinderVolumeFTP.class.php';
                    break;
            }
            $r['accessControl'] = 'access'; // disable and hide dot starting files (OPTIONAL)
        }
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();

    }
}


















//        
//        
//        /**
// * Smart logger function
// * Demonstrate how to work with elFinder event api
// *
// * @param  string   $cmd       command name
// * @param  array    $result    command result
// * @param  array    $args      command arguments from client
// * @param  elFinder $elfinder  elFinder instance
// * @return void|true
// * @author Troex Nevelin
// **/
//function logger($cmd, $result, $args, $elfinder) {
//
//
//	$log = sprintf("[%s] %s: %s \n", date('r'), strtoupper($cmd), var_export($result, true));
//	$logfile = '../files/temp/log.txt';
//	$dir = dirname($logfile);
//	if (!is_dir($dir) && !mkdir($dir)) {
//		return;
//	}
//	if (($fp = fopen($logfile, 'a'))) {
//		fwrite($fp, $log);
//		fclose($fp);
//	}
//	return;
//
//	foreach ($result as $key => $value) {
//		if (empty($value)) {
//			continue;
//		}
//		$data = array();
//		if (in_array($key, array('error', 'warning'))) {
//			array_push($data, implode(' ', $value));
//		} else {
//			if (is_array($value)) { // changes made to files
//				foreach ($value as $file) {
//					$filepath = (isset($file['realpath']) ? $file['realpath'] : $elfinder->realpath($file['hash']));
//					array_push($data, $filepath);
//				}
//			} else { // other value (ex. header)
//				array_push($data, $value);
//			}
//		}
//		$log .= sprintf(' %s(%s)', $key, implode(', ', $data));
//	}
//	$log .= "\n";
//
//	$logfile = '../files/temp/log.txt';
//	$dir = dirname($logfile);
//	if (!is_dir($dir) && !mkdir($dir)) {
//		return;
//	}
//	if (($fp = fopen($logfile, 'a'))) {
//		fwrite($fp, $log);
//		fclose($fp);
//	}
//}
//
//
///**
// * Simple logger function.
// * Demonstrate how to work with elFinder event api.
// *
// * @package elFinder
// * @author Dmitry (dio) Levashov
// **/
//class elFinderSimpleLogger {
//
//	/**
//	 * Log file path
//	 *
//	 * @var string
//	 **/
//	protected $file = '';
//
//	/**
//	 * constructor
//	 *
//	 * @return void
//	 * @author Dmitry (dio) Levashov
//	 **/
//	public function __construct($path) {
//		$this->file = $path;
//		$dir = dirname($path);
//		if (!is_dir($dir)) {
//			mkdir($dir);
//		}
//	}
//
//	/**
//	 * Create log record
//	 *
//	 * @param  string   $cmd       command name
//	 * @param  array    $result    command result
//	 * @param  array    $args      command arguments from client
//	 * @param  elFinder $elfinder  elFinder instance
//	 * @return void|true
//	 * @author Dmitry (dio) Levashov
//	 **/
//	public function log($cmd, $result, $args, $elfinder) {
//		$log = $cmd.' ['.date('d.m H:s')."]\n";
//
//		if (!empty($result['error'])) {
//			$log .= "\tERROR: ".implode(' ', $result['error'])."\n";
//		}
//
//		if (!empty($result['warning'])) {
//			$log .= "\tWARNING: ".implode(' ', $result['warning'])."\n";
//		}
//
//		if (!empty($result['removed'])) {
//			foreach ($result['removed'] as $file) {
//				// removed file contain additional field "realpath"
//				$log .= "\tREMOVED: ".$file['realpath']."\n";
//			}
//		}
//
//		if (!empty($result['added'])) {
//			foreach ($result['added'] as $file) {
//				$log .= "\tADDED: ".$elfinder->realpath($file['hash'])."\n";
//			}
//		}
//
//		if (!empty($result['changed'])) {
//			foreach ($result['changed'] as $file) {
//				$log .= "\tCHANGED: ".$elfinder->realpath($file['hash'])."\n";
//			}
//		}
//
//		$this->write($log);
//	}
//
//	/**
//	 * Write log into file
//	 *
//	 * @param  string  $log  log record
//	 * @return void
//	 * @author Dmitry (dio) Levashov
//	 **/
//	protected function write($log) {
//
//		if (($fp = @fopen($this->file, 'a'))) {
//			fwrite($fp, $log."\n");
//			fclose($fp);
//		}
//	}
//
//
//} // END class 
//
//
///**
// * Simple function to demonstrate how to control file access using "accessControl" callback.
// * This method will disable accessing files/folders starting from  '.' (dot)
// *
// * @param  string  $attr  attribute name (read|write|locked|hidden)
// * @param  string  $path  file path relative to volume root directory started with directory separator
// * @return bool|null
// **/
//function access($attr, $path, $data, $volume) {
//	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
//		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
//		:  null;                                    // else elFinder decide it itself
//}
//
///**
// * Access control example class
// *
// * @author Dmitry (dio) Levashov
// **/
//class elFinderTestACL {
//
//	/**
//	 * make dotfiles not readable, not writable, hidden and locked
//	 *
//	 * @param  string  $attr  attribute name (read|write|locked|hidden)
//	 * @param  string  $path  file path. Attention! This is path relative to volume root directory started with directory separator.
//	 * @param  mixed   $data  data which seted in 'accessControlData' elFinder option
//	 * @param  elFinderVolumeDriver  $volume  volume driver
//	 * @return bool
//	 * @author Dmitry (dio) Levashov
//	 **/
//	public function fsAccess($attr, $path, $data, $volume) {
//
//		if ($volume->name() == 'localfilesystem') {
//			return strpos(basename($path), '.') === 0
//				? !($attr == 'read' || $attr == 'write')
//				: $attr == 'read' || $attr == 'write';
//		}
//
//		return true;
//	}
//
//} // END class 
//
//$acl = new elFinderTestACL();
//
//function validName($name) {
//	return strpos($name, '.') !== 0;
//}
//
//
//$logger = new elFinderSimpleLogger('../files/temp/log.txt');