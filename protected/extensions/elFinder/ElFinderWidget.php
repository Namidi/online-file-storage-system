<?php
/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
class ElFinderWidget extends CWidget
{
    /**
     * Client settings.
     * More about this: https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
     * @var array
     */
    public $settings = array();
    public $connectorRoute = 'admin/elfinder/connector2';//false
    private $assetsDir;

    public function init()
    {
        $dir = dirname(__FILE__) . '/assets';
        $this->assetsDir = Yii::app()->assetManager->publish($dir, false, -1, true);
        $cs = Yii::app()->getClientScript();

        // jQuery and jQuery UI
        $cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
        $cs->registerCssFile($this->assetsDir . '/smoothness/jquery-ui-1.8.21.custom.css');
//        $cs->registerCssFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/smoothness/jquery-ui.css');
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('jquery.ui');
//        $cs->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js');

        // elFinder CSS
        $cs->registerCssFile($this->assetsDir . '/css/elfinder.min.css');
        $cs->registerCssFile($this->assetsDir . '/css/theme.css');

        // elFinder JS
        
        
        if(Yii::app()->user->isAdmin()){
            $cs->registerScriptFile($this->assetsDir . '/js/cfg_admin.full.js');
        }elseif(Yii::app()->user->isDocent()){
            $cs->registerScriptFile($this->assetsDir . '/js/cfg_docent.full.js');
        }else{
            $cs->registerScriptFile($this->assetsDir . '/js/cfg_student.full.js');
        }
        
        // elFinder translation
        $cs->registerScriptFile($this->assetsDir . '/js/i18n/elfinder.ru.js');
//        $cs->registerScriptFile($this->assetsDir . '/js/functions.js');

        // set required options
        if(empty($this->connectorRoute))
            throw new CException('$connectorRoute must be set!');
        $this->settings['url'] = Yii::app()->createUrl($this->connectorRoute);
        $this->settings['lang'] = Yii::app()->language;
        $this->settings['langeee'] = Yii::app()->language;
//        $this->settings['handlers'] = 'select : function(event, elfinderInstance) {console.log(event.data);console.log(event.data.selected);}';
        
    }

    public function run()
    {
        $ajaxurl = Yii::app()->createUrl('site/uploads');
        $id = $this->getId();
        $settings = CJavaScript::encode($this->settings);
        $cs = Yii::app()->getClientScript();
        $cs->registerScript('elFinder', "
            var elfinder = $('#$id').elfinder($settings).elfinder('instance');
                
                elfinder.bind('upload', function(event){
                     setTimeout(function(){
                        elfinder.exec('open',event.data.added[0].phash)
                        $.ajax({
                            url: '$ajaxurl',
                            cache: false,
                            success: function(html){
                              $('#uploaded').html(html);
                            }
                          });
                    }, 500);
                });
                elfinder.bind('remove', function(event){
                     setTimeout(function(){
                        $.ajax({
                            url: '$ajaxurl',
                            cache: false,
                            success: function(html){
                              $('#uploaded').html(html);
                            }
                          });
                    }, 500);
                });
                
                elfinder.bind('add', function(event){
                     setTimeout(function(){
                        elfinder.exec('open',event.data.added[0].phash)
                    }, 300);
                });
                

                ");
        echo "<div id=\"$id\"></div>";
    }

}
