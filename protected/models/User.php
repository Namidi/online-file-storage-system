<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 */
class User extends CActiveRecord
{
    const ADMIN_ROLE = 1;
    const DOCENT_ROLE = 2;
    const STUDENT_ROLE = 3;
    public $new_password;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}
	public function get_user($email){
		$user = Yii::app()->db->createCommand()
		->select('*')
		->from('tbl_user')
		->where('email=:email', array(':email'=>$email))
		->queryRow();
		return $user;
	}
	public function add_fp_token($email){
		$token = md5(uniqid('ict', true));
		$res = Yii::app()->db->createCommand()
		->select('*')
		->from('forgot_password_tokens')
		->where('email=:email', array(':email' => $email))
		->queryRow();
		if($res){
			$command = Yii::app()->db->createCommand();
			$command->update('forgot_password_tokens', array(
				'token' => $token,
				'expire' => time() + 3600
			), 'email=:email', array(':email' => $email));
		}else{
			$command = Yii::app()->db->createCommand();
			
			$data = array(
				'email' => $email,
				'token' => $token,
				'expire' => time() + 3600
			);
			$command->insert('forgot_password_tokens', $data);
			
		}
		return $token;
	}
	public function delete_token($token){
		$command = Yii::app()->db->createCommand();
		$command->delete('forgot_password_tokens', 'token=:token', array(':token' => $token));
	}
	public function change_password($email, $password){
		$command = Yii::app()->db->createCommand();
		$command->update('tbl_user', array(
			'password'=>md5($password),
		), 'email=:email', array(':email'=>$email));
		
	}
	public function is_token_exist($token){
		$expire = time();
		$data = Yii::app()->db->createCommand()
		->select('*')
		->from('forgot_password_tokens')
		->where('token=:token and expire>'.time(), array(':token' => $token))
		->queryRow();
		return $data;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, role', 'required'),
                                                      array('email', 'email'),
                                                      array('email', 'unique'),
                                                      array('password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('username, new_password, email, password, role', 'safe')
		);
//		return array(
//			array('username, password, email, role', 'required'),
//			array('username, password, email', 'length', 'max'=>128),
//			// The following rule is used by search().
//			// @todo Please remove those attributes that should not be searched.
//			array('id, username, password, email', 'safe',  'on'=>'search'),
//			array('role', 'safe'),
//			array('email', 'email'),
//		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'role' => 'Group',
			'new_password' => 'Retry password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
                
        
               public static function getRoles(){
                    return array(
                        1 => 'Admin',
                        2 => 'Docent',
                        3 => 'Student'
                    );
                }
                
                public function getRole(){
                    $role_list = self::getRoles();
                    if(isset($role_list[$this->role])){
                        return $role_list[$this->role];
                    }else{
                        return false;
                    }
                }
    
    
                /**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
                
                
                
    public function validatePassword($password) {
		return $this->hashPassword($password) === $this->password;
	}

	public function hashPassword($password) {
		return md5($password);
	}

//	public function validatePassword($password)
//	{
//		return CPasswordHelper::verifyPassword($password,$this->password);
//	}
//
//	/**
//	 * Generates the password hash.
//	 * @param string password
//	 * @return string hash
//	 */
//	public static function hashPassword($password)
//	{
//		return CPasswordHelper::hashPassword($password);
//	}
}
