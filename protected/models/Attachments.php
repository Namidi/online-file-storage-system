<?php

/**
 * This is the model class for table "tbl_attachments".
 *
 * The followings are the available columns in table 'tbl_attachments':
 * @property integer $id
 * @property string $path
 * @property integer $size
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Attachments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_attachments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path, size', 'required'),
			array('size, user_id', 'numerical', 'integerOnly'=>true),
			array('path', 'length', 'max'=>255),
            array('user_id', 'default', 'value'=>Yii::app()->User->id ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, path, size, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'path' => 'Path',
			'size' => 'Size',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attachments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function topSearch($value, $user_id, $std = false)
	{
		if(!$std)
		{
			$sql = "SELECT id,path,type,size,date,user_id FROM tbl_attachments WHERE path LIKE '%$value%' ";
			$command = Yii::app()->db->createCommand($sql)->queryAll();
		
		}
		else
		{
			$sql = "SELECT j.*
					FROM tbl_attachments  j 
					INNER JOIN tbl_user u ON u.id =j.user_id AND ((u.role = 3 AND j.user_id = '$user_id' ) OR u.role = 2 OR u.role = 1)
					WHERE j.path LIKE '%$value%' ";
			$command = Yii::app()->db->createCommand($sql)->queryAll();
			
		}
		
	
		/*$result = Yii::app()->db->createCommand()
				->select('*')
				->from('tbl_attachments')
				->where('id=:id', array(':id'=>159))
				->queryRow();*/
		return $command;
	}
	public function delete($node)
	{
		$n_node = $node.'/';
		$sql = "DELETE FROM tbl_attachments WHERE path LIKE '$n_node%' OR path='$node' ";
		Yii::app()->db->createCommand($sql)->execute();
	
	}
	
	public function selectStdId($id,$path,$std)
	{
		
		if($std){
			$sql = "SELECT *
					FROM tbl_attachments 
					
					WHERE  path IN ($path)  AND user_id IN (SELECT id FROM tbl_user WHERE role=2 OR role=1) 
					OR (path IN ($path)  AND user_id = '$id' )";
		}	
		else
		{
			$sql = "SELECT *
					FROM tbl_attachments 
					
					WHERE  path IN ($path)";
		}
		
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		return $command;
		
	}
	
	public function select_dirContine($dir_contine){
		/*$row = Yii::app()->db->createCommand(array(
				'select' => array('*'),
				'from' => 'tbl_attachments',
				'where' => 'path=:path',
				'params' => $dir_contine
			))->queryAll();*/
		$sql = "SELECT * FROM tbl_attachments WHERE path IN ($dir_contine) ";
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		return $command;	
	}
	
	public function exist_fileName_model($name){
		$sql = "SELECT * FROM tbl_attachments WHERE path='$name' ";
		$command = Yii::app()->db->createCommand($sql)->queryRow();
		return $command;
	
	}
	
	public function all_upload_size($user_id){
		$sql = "SELECT sum(size) AS size FROM tbl_attachments WHERE user_id='$user_id' ";
		$command = Yii::app()->db->createCommand($sql)->queryRow();
		return $command;
	
	}
	
	public function select_user_id($node)
	{
		$sql = "SELECT user_id,type,path FROM tbl_attachments WHERE path='$node' ";
		$command = Yii::app()->db->createCommand($sql)->queryRow();
		return $command;
	}
	public function nodeExist($node)
	{
		$sql = "SELECT * FROM tbl_attachments WHERE path='$node' ";
		$command = Yii::app()->db->createCommand($sql)->queryRow();
		return $command;
	}
	public function prmDownloadFile($user_id,$file_name){
		$sql = "SELECT *
					FROM tbl_attachments 
					
					WHERE  path = '$file_name' AND type = 1  AND user_id IN (SELECT id FROM tbl_user WHERE role=2 OR role=1) 
					OR (path='$file_name'  AND user_id = '$user_id' )";
	
	
		//$sql = "SELECT * FROM tbl_attachments WHERE path='$file_name' AND user_id='$user_id'   ";
		$command = Yii::app()->db->createCommand($sql)->queryRow();
		return $command;
	
	}
	
	public function save_data($up_data){
		foreach($up_data as $val){
			$path = $val['path'];
			$user_id = $val['user_id'];
			$size = $val['size'];
			$type = $val['type'];
			$date = $val['date'];
			$sql = "INSERT INTO tbl_attachments (path,user_id,size,type,date)  VALUES ('$path','$user_id','$size','$type','$date')";
			$command = Yii::app()->db->createCommand($sql)->execute();
		}
	
	}
	
}
