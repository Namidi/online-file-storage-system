<div class="row">
    <div class="col-md-5">
        <nav role="navigation">
		<p>
<?php
/* @var $this UserController */
/* @var $model User */

//$this->breadcrumbs=array(
//	'Users'=>array('index'),
//	'Manage',
//);

$this->widget('zii.widgets.CMenu',array(
		'activeCssClass'=>'active',
        'id'=>'navigation',
		'encodeLabel'=>false, 
		'htmlOptions'=>array('class'=>'nav nav-pills nav-justified  '),
       'items'=>array(
//	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User(s)', 'url'=>array('create'),'itemOptions'=>array('class'=>''),'linkOptions'=>array('class'=>' green hover btn-w-m btn btn-primary','role'=>'menuitem')),
	//array('label'=>'Advanced Search', 'url'=>array('admin#'), 'itemOptions'=>array('class'=>'search-button')),
)));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
</p>
</nav>
</div>
</div>



<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<div class="col-lg-9" >
<div class="ibox float-e-margins">
<div class="ibox-title">
           <h3><strong>Search</strong></h3>
                            
    </div>
<div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array(
        'class'=>'form-inline',
		'role'=>'form'
    )
)); ?>

<!--	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>-->

	
	<div class="form-group">
		<?php // echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>30,'placeholder'=>'Email','maxlength'=>128,'class'=>'form-control')); ?>
	</div>
	<div class="form-group">
	<h3>OR</h3>
	</div>
	<div class="form-group">
		<?php //echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'placeholder'=>'Username','maxlength'=>128,'class'=>'form-control')); ?>
	</div>

	

	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
			<?php echo CHtml::submitButton('Search',array('class'=>'btn btn-sm btn-white')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div>
</div>

<!-- <h1>Manage Users</h1> -->

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
    
</div><!-- search-form -->
<div class="col-lg-12" >
<div class="ibox float-e-margins">
<div class="ibox-title">
           <h3><strong>Grid options</strong></h3>
                            
    </div>
<div class="ibox-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'itemsCssClass' => 'table table-bordered table-striped',
	'dataProvider'=>$model->search(),
	
	'pager'=> array('htmlOptions' => array(
					'class' => 'paginator'
				)),
	//'pagerCssClass'=>'pagination pagination-centered',
//	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'email',
            array(            // display 'author.username' using an expression
            'name' => 'role',
            'value' => '$data->getRole()',
            'type' => 'html',
        ),
                                    
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			    'buttons' => array(
                        'delete' => array(
                            
                            'options' => array(
                                'confirm' => 'Are you sure you want to delete this item?',
                                'ajax' => array(
                                    'type' => 'POST',
                                    //'dataType'=>'json',
                                    'url' => "js:$(this).attr('href')",
                                    'success' => 'function(data){
                                         window.history.go(0); 
										 
                                    }'
                                ),
							),
						),
					),	
		),
	),
)); ?>
</div>
</div>
</div>