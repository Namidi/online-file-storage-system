<div class="row">
    <div class="col-md-5">
        <nav role="navigation">
		<p>
<?php
/* @var $this UserController */
/* @var $model User */

//$this->breadcrumbs=array(
//	'Users'=>array('index'),
//	'Create',
//);
//echo CHtml::openTag('div', array('class' => 'bs-navbar-top-example'));

$this->widget('zii.widgets.CMenu',array(
		
        'id'=>'navigation',
		'encodeLabel'=>false, 
		'htmlOptions'=>array('class'=>'nav nav-pills nav-justified  '),
       'items'=>array(
//	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage Users', 'url'=>array('admin'),'linkOptions'=>array('class'=>' green hover btn-w-m btn btn-primary','role'=>'menuitem')),
)));
//echo CHtml::closeTag('div');
?>
</p>
</nav>
</div>
</div>


<?php $this->renderPartial('_form', array('model'=>$model, 'roles'=>$roles)); ?>