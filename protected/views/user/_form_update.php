<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>


<!-- <div class="row">
<div class="col-sm-4">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'user-form',
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row"> 
		
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'username'); ?>
	
	</div> 

    <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'new_password'); ?>
		<?php echo $form->passwordField($model,'new_password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'new_password'); ?>
	</div>
        
   <?php if(Yii::app()->user->isAdmin()){?>
   
	<div class="row">
	<div class="col-sm-4">
		<?php echo $form->labelEx($model,'role'); ?>
		<?php echo $form->dropDownList($model, 'role', $roles, array('prompt'=>'Select Group','class'=>'form-control')); ?>
		<?php echo $form->error($model,'Group'); ?>
	</div>
	</div>
                    <?php }?>
	<div class="row buttons ">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
</div>

</div><!-- form -->

    
<?php echo $form->errorSummary($model); ?>
				<div class="col-lg-6" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Update User</h5>
                            
                        </div>
				
                        <div class="ibox-content">
                          <!--  <form class="form-horizontal">  -->
                                <?php $form=$this->beginWidget('CActiveForm', array(
									'id'=>'user-form',
									'htmlOptions'=>array(
										'class'=>'form-horizontal',
										'role'=>'form'
									),
																// Please note: When you enable ajax validation, make sure the corresponding
									// controller action is handling ajax validation correctly.
									// There is a call to performAjaxValidation() commented in generated controller code.
									// See class documentation of CActiveForm for details on this.
									'enableAjaxValidation'=>false,
								)); ?>
                                <div class="form-group"><label class="col-lg-2 control-label" style="text-align: left; "><?php echo $form->labelEx($model,'username'); ?></label>
                                    <div class="col-lg-10">
										<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
										<?php echo $form->error($model,'username'); ?>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label" style="text-align: left; "><?php echo $form->labelEx($model,'email'); ?></label>

                                    <div class="col-lg-10">
										<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
										<?php echo $form->error($model,'email'); ?>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label" style="text-align: left; "><?php echo $form->labelEx($model,'password'); ?></label>

                                    <div class="col-lg-10">
										
										<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
										<?php echo $form->error($model,'password'); ?>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label" style="text-align: left; ">Password</label>
                                    <div class="col-lg-10">
										
										<?php echo $form->passwordField($model,'new_password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
										<?php echo $form->error($model,'new_password'); ?>
									</div>
								</div>
								<?php if(Yii::app()->user->isAdmin()){?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="text-align: left; ">Group</label>
									<div class="col-lg-10">
										<div class="input-group m-b">
											<div class="input-group-btn">
												<?php echo $form->dropDownList($model, 'role', $roles, array('class'=>' btn btn-white dropdown-toggle','data-toggle'=>'dropdown','type'=>'button')); ?>
												<?php echo $form->error($model,'Group'); ?>
                                         	</div>
											
										</div>
									</div>	
								</div>
                                 <?php }?>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
										<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-sm btn-white')); ?>
                                       <!-- <button class="btn btn-sm btn-white" type="submit">Sign in</button> -->
                                    </div>
                                </div>
								<?php $this->endWidget(); ?>
                          <!--  </form> -->
                        </div>
                    </div>
				</div>
			

