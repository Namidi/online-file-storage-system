<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<div class="col-lg-6" >
	<div class="ibox float-e-margins">
		<div class="ibox-title">
           <h3><strong>Create User</strong></h3>
                            
        </div>
		
		
			<div class="ibox-content">
		
			
				
			<!--	<h1>Create User</h1>  -->
				<?php
				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'user-form',
					'enableAjaxValidation' => false,
					'htmlOptions'=>array('class'=>'form-horizontal')
						));
				?>
				<?php echo $form->errorSummary($model); ?>
		<!--		<p class="note">Fields with <span class="required">*</span> are required.</p>  -->

				
				<div class="form-group"><label class="col-lg-2 control-label" for="User_email" style="text-align: left; ">Email</label>
					<div class="col-lg-10">
						<?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128,'class'=>'form-control','placeholder'=>'Email')); ?>
						<?php echo $form->error($model, 'email'); ?>
					</div>
               
					
					
				</div>
				    <div class="form-group">
						<label class="col-sm-2 control-label" for="User_role" style="text-align: left; ">Group</label>
						<div class="col-lg-10">
							<div class="input-group m-b">
								<div class="input-group-btn">
									<?php echo $form->dropDownList($model, 'role', $roles, array('class'=>' btn btn-white dropdown-toggle','data-toggle'=>'dropdown','type'=>'button')); ?>
									<?php echo $form->error($model, 'Group'); ?>
								</div>
								
							</div>
						</div>	
					</div>

					
					
				

					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-sm btn-white')); ?>
						   <!-- <button class="btn btn-sm btn-white" type="submit">Sign in</button> -->
						</div>
                    </div>
					
			
			
		
			</div>
	</div>
	<?php $this->endWidget(); ?>
	<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h3 class="inline"><strong>Import Multiple Users  </strong></h3>
				<span>
					&nbsp;&nbsp;You can only upload a .csv file
				</span>
                            
			</div>
			<div class="ibox-content">
			<!--	<form id="my-awesome-dropzone" class="dropzone dz-clickable" action="#">
                    <div class="dropzone-previews"></div>
                    <button type="submit" class="btn btn-primary pull-right">Submit this form!</button>
					<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
			
				-->
				
				<?php
			
				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'users-import',
					'enableAjaxValidation' => false,
					'htmlOptions' => array('enctype' => 'multipart/form-data','class'=>'dropzone dz-clickable','id'=>'my-awesome-dropzone'),
						));
				?>
				<button type="submit" id="sub" class="btn btn-primary pull-right">Submit this form!</button>
			<!--	<p class="note">You can only upload <span class="required">.csv</span> file</p>
				<div class="row">
					<?php echo CHtml::label('Import users', ''); ?>
					<?php echo CHtml::FileField('import_list', '',array('class'=>'dz-default dz-message')); ?>
				</div>
				<div class="row buttons">
					<?php echo CHtml::submitButton('Import' ,array('class' => 'btn btn-primary pull-right')); ?>
				</div>
				<?php $this->endWidget(); ?>
			
				<!--</form> -->
			</div>
	</div><!-- form -->
	
</div>	

   <script>
        $(document).ready(function(){
			//  autoProcessQueue: false;
			//uploadMultiple: true;
           
			Dropzone.options.myAwesomeDropzone = {
				//autoProcessQueue: false,
			  paramName: "import_list", // The name that will be used to transfer the file
			/*  maxFilesize: 2, // MB
			  accept: function(file, done) {
				if (file.name == "justinbieber.jpg") {
				  done("Naha, you don't.");
				}
				else { done(); }
			  }*/
			};
		
			

     });
    </script>
	