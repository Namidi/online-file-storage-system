
<div class="row">
    <div class="col-md-5">
        <nav role="navigation">
		<p>
		
<?php
/* @var $this UserController */
/* @var $model User */

//$this->breadcrumbs=array(
//	'Users'=>array('index'),
//	$model->id=>array('view','id'=>$model->id),
//	'Update',
//);


if(Yii::app()->user->isAdmin()){
    $this->widget('zii.widgets.CMenu',array(
		'activeCssClass'=>'active',
        'id'=>'navigation',
		'encodeLabel'=>false, 
		'htmlOptions'=>array('class'=>'nav nav-pills nav-justified  '),
       'items'=>array(
    //	array('label'=>'List User', 'url'=>array('index')),
            array('label'=>'Create User(s)', 'url'=>array('create'),'itemOptions'=>array('class'=>''),'linkOptions'=>array('class'=>' green hover btn-w-m btn btn-primary','role'=>'menuitem')),
			array('label'=>'Manage Users', 'url'=>array('admin'),'linkOptions'=>array('class'=>' green hover btn-w-m btn btn-primary','role'=>'menuitem')),
            //array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id),'itemOptions'=>array('class'=>''),'linkOptions'=>array('class'=>' green hover btn btn-w-m btn-primary','role'=>'menuitem')),
			
		)	
          
       
	 ));	
		

    

			
    //	array('label'=>'Manage User', 'url'=>array('admin')),
   
	
	
	

}
?>
</p>
</nav>
</div>
</div>
<!-- <h1>Update User <?php echo $model->id; ?></h1>-->


<?php if(Yii::app()->user->hasFlash('success')): ?>
 
<div class="flash-success">

    <?php echo Yii::app()->user->getFlash('success'); ?>
	
</div>
 
<?php endif; ?>

<?php echo $this->renderPartial('_form_update', array('model'=>$model, 'roles'=>$roles)); ?>


