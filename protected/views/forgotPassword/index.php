<div class="row">
	<div class="col-sm-6"><h3 class="m-t-none m-b">Forgot Password</h3>
		<?php if(!isset($success)){ ?>
		<form method="post" action="">
			<div class="form-group">
				<?php if(isset($email_error)){ ?>
				<p style="color:red;">This email is not in our DB.</p>
				<?php } ?>
				<input type="email" name="email" placeholder="Enter email" class="form-control" />
			</div>
			
			<div>
				<button name="forgot" class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Send</strong></button>
				
			</div>
		</form>
		<?php }else{ ?>
			<p style="color:green;">Reset password link sent to your email</p>
		<?php }	?>
	</div>
	
</div>