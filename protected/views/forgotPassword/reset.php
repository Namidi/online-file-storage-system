<div class="row">
	<div class="col-sm-6"><h3 class="m-t-none m-b">Forgot Password</h3>
		<form method="post" action="">
		
			<?php if(isset($passwords_changed)){ ?>
				<p style="color:green;">Your password has been changed.</p>
				<script>
					setTimeout(function(){ window.location.href="<?php echo Yii::app()->params['domain']; ?>" }, 3000);
				</script>
			<?php } ?>
			<?php if(isset($empty_password)){ ?>
				<p style="color:red;">Please Fill passwords.</p>
			<?php } ?>
			<?php if(isset($not_equal_password)){ ?>
				<p style="color:red;">Passwords should be equal.</p>
			<?php } ?>
			<div class="form-group">
				<input type="password" name="password" placeholder="Password" class="form-control">
			</div>
			<div class="form-group">
				<input type="password" name="re_password" placeholder="Pepeat Password" class="form-control">
			</div>
			<div>
				<button name="reset" class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Reset</strong></button>
				
			</div>
		</form>
	</div>
	
</div>