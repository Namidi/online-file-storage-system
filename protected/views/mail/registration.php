<table style="width: 100%">
    <tr>
        <td>
            <!--<p>
                From: <?php echo $from?><br />
                To: <?php echo $to?><br />
                Subject: <?php echo $subject?>-->
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table style="border: solid 1px; border-color: #cccccc; width: 100%">
                <tr>
                    <td>
                        <div style="margin: 5px 0px 5px 0px;">
                            <p>Beste Gebruiker,</p>
                            <p>Zoals mevr. Feurich al aangaf tijdens de introductie week en het openingscollege, is het de bedoeling dat de UNASAT gebruik zal maken van een Online File Storage System (OFSS). Met dit systeem zijn alle benodigde studiebestanden permanent online. Via deze email worden de inloggegevens opgestuurd voor het UNASAT OFFS.</p>
                            <p>
                                Login gegevens<br />
                                Email: <?php echo $email?><br />
                                Password:  <?php echo $password?><br />
                                Website: http://www.unasat.org/inlogofss.html
                            </p>
                            <p>Na het inloggen met bovengenoemde gegevens, bent u in staat uw wachtwoord te wijzigen. </p>
                            <p>Met vriendelijke groet,</p>
                            <p>Het UNASAT Team.</p>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>