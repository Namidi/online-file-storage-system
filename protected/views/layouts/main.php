<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>  
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/font-awesome/css/font-awesome.css" rel="stylesheet">
     <!-- Data Tables -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/animate.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/style.css" rel="stylesheet">
	
	
	<link href="<?php echo Yii::app()->baseUrl; ?>/jstree/themes/default/style.min.css" rel="stylesheet" />
	<link href="<?php echo Yii::app()->baseUrl; ?>/jstree/themes/default/custom.css" rel="stylesheet" /> 
	<!-- Mainly scripts -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/jquery-2.1.1.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Data Tables -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/dataTables/dataTables.tableTools.min.js"></script>
	<!-- Custom and plugin javascript -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/inspinia.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/pace/pace.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/jquery.form.js"></script> 
	
	    <!-- Page-Level Scripts -->
		<!-- dropzone -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/dropzone/dropzone.js"></script>	
	 <!-- blueimp gallery -->
	 
	 
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
	<script>
	$(document).ready(function(){
		$('#page-wrapper').css('min-height', $(window).height());
	});
	</script>

	<style>
		body.DTTT_Print {
			background: #fff;

		}
		.DTTT_Print #page-wrapper {
			margin: 0;
			background:#fff;
		}

		button.DTTT_button, div.DTTT_button, a.DTTT_button {
			border: 1px solid #e7eaec;
			background: #fff;
			color: #676a6c;
			box-shadow: none;
			padding: 6px 8px;
		}
		button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
			border: 1px solid #d2d2d2;
			background: #fff;
			color: #676a6c;
			box-shadow: none;
			padding: 6px 8px;
		}

		.dataTables_filter label {
			margin-right: 5px;

		}
	</style>
</head>
<body>
<div id="blueimp-gallery" class="blueimp-gallery">
	<div class="slides"></div>
	<h3 class="title"></h3>
	
	<a class="close">×</a>
	
	
</div>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<span class="clear">
								<span class="block m-t-xs"> 
									<strong style="word-wrap:break-word;" class="font-bold"><?php echo isset(Yii::app()->user->email)?Yii::app()->user->email:'';; ?></strong>
								</span>
								<span class="text-muted text-xs block">
									<?php if(Yii::app()->user->isDocent()){ ?>
										Docent
									<?php }elseif(Yii::app()->user->isStudent()){ ?>
										Student
									<?php }elseif(Yii::app()->user->isAdmin()){ ?>
										Admin
									<?php } ?>
								<!--	<b class="caret"></b> -->
								</span> 
							</span>
						 </a>
					</div>
				</li>

							
							<?php $baseUrl = Yii::app()->baseUrl; if(!Yii::app()->user->isGuest){ ?>
							 <li>
                                    <a href="<?php echo Yii::app()->baseUrl; ?>" title="">
                                      <!--  <img class="svg" alt="" src="<?php echo $baseUrl?>/images/personal.svg" /> -->
									  <i class="fa fa-diamond"></i>
                                       <span class="nav-label">Home</span> 
                                    </a>
                             </li>
							
							
							 <li>
                                    <a href="<?php echo $this->createUrl('user/update', array('id' => Yii::app()->user->id)); ?>" title="">
                                      <!--  <img class="svg" alt="" src="<?php echo $baseUrl?>/images/personal.svg" /> -->
									  <i class="fa fa-user-md"></i>
                                       <span class="nav-label">Profile</span> 
                                    </a>
                             </li>
							 
							 <?php } ?>
							<?php $baseUrl = Yii::app()->baseUrl; if(Yii::app()->user->isAdmin()) { ?>	
                                <li>
                                    <a href="<?php echo $this->createUrl('user/admin'); ?>" title="">
                                      <!--  <img class="svg" alt="" src="<?php echo $baseUrl?>/images/users.svg" />   -->
										<i class="fa fa-users"></i>
                                        <span class="nav-label">Users</span> 
                                    </a>
                                </li>
								<?php } ?>
 <!--                               <li>
                                    <a id="logout" href="<?php echo $this->createUrl('site/logout'); ?>">
                                        <img class="svg" alt="" src="<?php echo $baseUrl?>/images/logout.svg" />
                                        LogOut
                                    </a>
                                </li>
-->								
					
					
                    
                    <div class="logo-element">
                        ICT
                    </div>
                <!--<li class="active">
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Main view</span></a>
                </li>
                <li>
                    <a href="minor.html"><i class="fa fa-th-large"></i> <span class="nav-label">Minor view</span> </a>
                </li>-->
            </ul>
        </div>
	</nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
			<?php if(!Yii::app()->user->isGuest){ ?>
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
               <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
             	   <div role="search" class="navbar-form-custom" >
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </div>
				</div>
				<ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="<?php echo $this->createUrl('site/logout'); ?>">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
			</nav>
			<?php } ?>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="m-t-lg">
                       <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
			<?php 
			
               $controller = Yii::app()->getController();
               $isHome = $controller->getId() === 'site' && $controller->getAction()->getId() === 'index';
           ?>
               	 <?php if (!Yii::app()->user->isAdmin() && !Yii::app()->user->isGuest && $isHome) { ?>
					<div class="pull-right">
						<span class="uploadded-size"><?php echo $this->uploadded_size ?>MB</span> of <strong><?php echo $this->upload_size_of ?>MB</strong> Free.
					</div>
				<?php } ?>	
            <div>
                <strong>Copyright</strong> UNASAT &copy; <?php echo date('Y'); ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
