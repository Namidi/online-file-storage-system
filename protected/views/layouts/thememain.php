<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        
        
        <!-- blueprint CSS framework -->
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
                <!-- jQuery and jQuery UI (REQUIRED) -->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">-->
        
        
        <!--<link rel="stylesheet" type="text/css" media="screen" href="/filemanager/css/jquery-ui-1.10.3.custom.min.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

         elFinder CSS (REQUIRED) 
        <link rel="stylesheet" type="text/css" media="screen" href="/filemanager/css/main.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/filemanager/css/elfinder.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/filemanager/css/theme.css">

         elFinder JS (REQUIRED) 
        <script type="text/javascript" src="/filemanager/js/functions.js"></script>
        <script type="text/javascript" src="/filemanager/js/elfinder.min.js"></script>

         elFinder translation (OPTIONAL) 
        <script type="text/javascript" src="/filemanager/js/i18n/elfinder.ru.js"></script>

         elFinder initialization (REQUIRED) 
        <script>
            $().ready(function() {
                var elf = $('#elfinder').elfinder({
                    url : '/filemanager/php/connector.php'  // connector URL (REQUIRED)
                    // lang: 'ru',             // language (OPTIONAL)
                }).elfinder('instance');
            });
        </script>-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="header">
            <a href="/" title="" id="cloud-logo">Cloud</a>
            
            <div id="logo-claim" style="display:none;"></div>
            
            
            <?php 
//            echo "<pre>";
//                print_r(Yii::app()->user);
//            echo "</pre>";
            ?>
            
            <?php
            if(!Yii::app()->user->isGuest){
                if(Yii::app()->user->role == 1){?>
                    <ul id="settings" class="svg">
                        <span id="expand">
                            <span id="expandDisplayName">admin</span>
                            <img class="svg" src="/filemanager/img/caret.svg" />
                        </span>
                        <div id="expanddiv">
                            <li>
                                <a href="<?php echo $this->createUrl('user/update', array('id'=>Yii::app()->user->id));?>" title="">
                                    <img class="svg" alt="" src="/filemanager/img/personal.svg" />
                                    Account
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->createUrl('user/admin');?>" title="">
                                    <img class="svg" alt="" src="/filemanager/img/users.svg" />
                                    Users
                                </a>
                            </li>
                            <li>
                                <a id="logout" href="<?php echo $this->createUrl('site/logout');?>">
                                    <img class="svg" alt="" src="/filemanager/img/logout.svg" />
                                    LogOut
                                </a>
                            </li>
                        </div>
                    </ul>
                <?php }else{?>
                    <ul id="settings" class="svg">
                        <span id="expand">
                            <span id="expandDisplayName"><?php echo Yii::app()->user->username?></span>
                            <img class="svg" src="/filemanager/img/caret.svg" />
                        </span>
                        <div id="expanddiv">
                            <li>
                                <a href="<?php echo $this->createUrl('user/update', array('id'=>Yii::app()->user->id));?>" title="">
                                    <img class="svg" alt="" src="/filemanager/img/personal.svg" />
                                    Account
                                </a>
                            </li>
                            <li>
                                <a id="logout" href="<?php echo $this->createUrl('site/logout');?>">
                                    <img class="svg" alt="" src="/filemanager/img/logout.svg" />
                                    LogOut
                                </a>
                            </li>
                        </div>
                    </ul>
                <?php }?>
            <?php }?>
        </div>
    
<div class="container" id="page">
    <?php 
//    echo "<pre>";
//        print_r($action);
//    echo "</pre>";
    ?>
    <?php echo $content; ?>
</div>

</body>
</html>

