<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UNASAT OFSS| Login</title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/animate.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
 
	<?php echo $content; ?>
    <!-- Mainly scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/jquery-2.1.1.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/layout_data/js/bootstrap.min.js"></script>
</body>
</html>
