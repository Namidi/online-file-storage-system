<?php
//chante isStudent to isAdmin
if(Yii::app()->user->isStudent()) {
	$hidden = 'hidden';
	$right_box = 'style="margin-left: 0px;"';
} else {
	$hidden = '';
	$right_box = '';
}
//var_dump($this->user_id = Yii::app()->user->getId());
?>

<div id="container" role="main">

	<div class="left_side hidden <?php echo $hidden; ?>">
		<!--<div class="infont col-md-3 col-sm-4"><a href="#"><i class="fa fa-times-circle"></i></a></div>-->
		<!--
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-bottom:10px;"></div>
		<span style="color:red;" class="images_upload_error"></span>
		<form id="files_form" method="post" action="" enctype="multipart/form-data">
			<label style="width:276px;margin-left:20px;" title=" Upload Files " for="inputImage" class="btn btn-primary">
				<input type="file"  name="file[]" id="inputImage" class="hide" multiple="multiple">
				<span class="upload_btn_title">Upload Files</span>
			</label>
			<input type="hidden" id="file_id"  name="file_id" value="" />
		</form>
		-->
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-top:10px;"></div>
		<div id="tree"></div>
	</div>
	<div id="data" style="margin-left: 0px;"  <?php //echo $right_box; ?> >
		<div class="content code" style="display:none;"><textarea id="code" readonly="readonly"></textarea></div>
		<div class="content folder" style="display:none;"></div>
		<div class="content image" style="display:none; position:relative;"><img src="" alt="" style="display:block; position:absolute; left:50%; top:50%; padding:0; max-height:90%; max-width:90%;" /></div>
		<div class="default" style="">
			<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />
		</div>
	</div>
</div>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.fileupload.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/jstree/js/jstree.js"></script>
<!--	<div class="row icons-box">
		<div class="infont col-lg-12" style="width:98%">
			<div class="ibox float-e-margins">
				<div class="ibox-content table-responsive">
					<ol class="breadcrumb">
						<li>
							<a  style="color:#1C84C6;text-decoration:underline;" class="open_dir">Home</a>
						</li>
					</ol>
					<a style="color:white; float: left;" class="btn btn-primary create_new_folder" href="">Create New Folder</a>
					<div style="float: left;">
						<form style="" id="files_form" method="post" action="" enctype="multipart/form-data">
							<label style="width:155px;margin-left:5px;" title=" Upload Files " for="inputImage" class="btn  btn_upl btn-primary">
								<input type="file" name="file[]" id="inputImage" class="hide" multiple="multiple">
									<span class="upload_btn_title">Upload Files</span>
							</label>
							<input type="hidden" id="file_id"  name="file_id" value="" />
						</form>
					</div>	
					<table class="table table-hover files_datatable">
						<thead>
							<tr>
								<th>Type</th>
								<th>Name</th>
								<th>Download/View</th>
								<th>Remove</th>
								<th>Size</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr class="'+root+'">
								<td class="first_column">
									<span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a>
								</td>
								<td>
									<input class="open_dir "   file_id="" oncontextmenu="return false;" value="" style="border: none; outline: none" readonly="radonly" />
								</td>
								<td>
									<a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id="><i class="download_item fa fa-file-zip-o" file_id="" ></i></a>
								</td>
								<td>
									<a><i file_id="" class="remove fa fa-times-circle"></i></a>
								</td>
								<td></td>
								<td></td>
							</tr>					
								
						</tbody>
					</table>	
					
				</div>			
			</div>			
		</div>
	</div>	
-->	
<script>

window.user_uploads = "<?php echo $user_uploads; ?>";
window.user_get_id = "<?php echo Yii::app()->user->getId(); ?>";
window.user_role = "<?php echo $user_role; ?>";
window.base_folder_path = '<?php echo Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR .'home' . DIRECTORY_SEPARATOR; ?>';
window.loader = '<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />';
$(document).ready(function(){ 
	get_folder_contine('/','home');
	function get_folder_contine(id,post_name){
		
		$.ajax({
				url:'http://www.unasat.org/ict_new/',
				type:'POST',
				//dataType:'json',
				data:{id:id,post_name:post_name},
				
				success:function(data){
					var d = $.parseJSON(data);
					//console.log(d);
					var dir_contain = d.dir_contain;
			
								var file_icons = {
									'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
									'png':'<a><i class="fa fa-file-image-o"></i></a>',
									'gif':'<a><i class="fa fa-file-image-o"></i></a>',
									'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
									'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
									'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
									'doc': '<a><i class="fa fa-file-word-o"></i></a>',
									'docx': '<a><i class="fa fa-file-word-o"></i></a>',
									'txt': '<a><i class="fa fa-file-text"></i></a>',
									'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
									'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
									'default': '<a><i class="fa fa-file-o"></i></a>',
								};
								//alert(d.content);
								role_path = false;
								d_content = d.content;
								if(d.content != "")
								{
									var arr = d.content.split('/');
									if(arr[3] && arr[3] == 'Student Uploads'){
										role_path = true;
									}
								}
								var html = '<div class="row icons-box">';
									html += '<div class="infont col-lg-12" style="width:98%">';
										html += '<div class="ibox float-e-margins">';
										html += '<div class="ibox-content table-responsive">';
										html += '<ol class="breadcrumb">';
											html += '<li><a file_id="/" style="color:#1C84C6;text-decoration:underline;" class="open_dir">Home</a></li>';
											var brcs = (d.content).split("/");
											var str = ''; 
											if(brcs.length){
												for(var i = 0; i < brcs.length; i++){
													if(i == brcs.length - 1){
														html += '<li class="active"style="color:#1ab394"><strong>' + brcs[i] + '</strong></li>';
													}else{
														str += (i == 0?'':'/') + brcs[i];
														html += '<li><a style="color:#1C84C6;text-decoration:underline;" class="open_dir" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
													}
												}
											}
			
										html += '</ol>';
										if(user_role == 2 || user_role == 1 || role_path != false  )
										{
											html += '<a style="color:white; float: left;" file_id = "' + d.content + '" class="btn btn-primary create_new_folder" href="">Create New Folder</a>';
										}
										html += '<div style="float: left;">';
			
										
	
				
											if(user_role == 2 || user_role == 1 || role_path != false  )
											{
												html += '<form style="" id="files_form" method="post" action="" enctype="multipart/form-data">';
													html += '<label style="width:155px;margin-left:5px;" title=" Upload Files " for="inputImage" class="btn  btn_upl btn-primary">';
													html += '<input type="file" name="file[]" id="inputImage" class="hide" multiple="multiple">';
													html += '<span class="upload_btn_title">Upload Files</span>';
													html += '</label>';
													html += '<input type="hidden" id="file_id"  name="file_id" value="" />';
												html += '</form>';
											}
										html += '</div>';
								    html += '<table class="table table-hover files_datatable">';
									html += '<thead>';
										html += '<tr>';
											html += '<th>Type</th>';
											html += '<th>Name</th>';
											html += '<th>Download/View</th>';
											html += '<th>Remove</th>';
											html += '<th>Size</th>';
											html += '<th>Date</th>';
										html += '</tr>';
									html += '</thead>';
									html += '<tbody>';
									
										for(var i in dir_contain){
											var root = '';
											if(i == 0){
												root = 'root';
											}
											
											html += '<tr class="'+root+'">';
												if(dir_contain[i].type == 'folder'){
													html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
													html += '<td><input class="open_dir "  type_f="falder" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
													//html += '<input class="hidden  file_name" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
															
												//	html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
													html +=	'<td><a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="download_item fa fa-file-zip-o" file_id="'+dir_contain[i].file_id+'" ></i></a></td>';
													if(user_role == 2 || user_role == 1 || role_path != false  ){
														
														html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													}
													else
													{
														html += '<td></td>';
														
													}
													html += '<td></td>';
												}else{
													//file_icons
													
													var ext = (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1);
													var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
													
													html += '<td class="first_column"><span style="display:none;">'+ (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1)+'</span>' + file_icon + '</td>';
													html += '<td>';
														html += '<input class="f_name"  type_f="file" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" />';
													//	html += '<div class="div_rename" style="display: none; position: absolute" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'"><input type="text" class="input_rename" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'" value="'+dir_contain[i].name+'" /></div>';
													html += '</td>';
													html +=	'<td>';
														html +=	'<a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="fa fa-save download_item"></i></a>';
														var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
														var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
														html +=	'<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + dir_contain[i].file_id+'" class="' + data_gallery_class + ' view" file_id="'+dir_contain[i].file_id+'"><i class="view_item fa fa-eye"></i></a>';
													
													html +=	'</td>';
													if(user_role == 2 || user_role == 1 || role_path != false  ){
														html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													}
													else
													{
														html += '<td></td>';
													}
													html += '<td>'+dir_contain[i].size+'</td>';
												}
												html += '<td>'+dir_contain[i].date+'</td>';
											html += '</tr>';
											
										}
									html += '</tbody>';
									
								html += '</table>';
								html += '</div></div></div></div>';
						
								if(d.file_type != 'file'){
									$('#data .default').html(html).show();
									setTimeout(function(){
										if( (opend_folder.node && opend_folder.node.id) ){
											var tmp = opend_folder.node.id;
										}else{
											var tmp = '/';
										}
										$('#tree').jstree("refresh_node", tmp);
									}, 1000);
									
										
									$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
					
					
					
					
					
				},
				error:function(){ alert("error"); }  

		});


	}






});
$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
									
//var opend_folder;
//alert(user_role);
/*$(function () {
	
	$(window).resize(function () {
		$('.files_datatable').css('width', '100%');
	}).resize();
	$(document).on('click','.open_dir', function(){
		var file_id = $(this).attr('file_id');
		if(file_id == '/'){
			$('#tree').jstree("refresh", true, true);
			$('#tree').jstree("refresh");
		}	
		function doSetTimeout_op(i, time,str,file_id) {
			setTimeout(function() { 
			$("#tree").jstree('open_node', str);
			$("[id='"+file_id+"_anchor']").trigger('dblclick');
			$("[id='"+file_id+"_anchor']").trigger('click');
			}, time);
			
		}
		var res = file_id.split("/");
		$("#tree").jstree('open_node','/');
		var str = '';
		var time = 0;
		for(var i = 0; i< res.length; i++){
			str += (i == 0?'':'/') + res[i];
			time += 600;
			doSetTimeout_op(i, time,str,file_id);
		}
		
		
		
		return false;
	});
	
	$(document).on('click', '.remove', function(){
		result = confirm('Do you really want to delete this item?');
		
		if(result == true){
			var file_id_ = $(this).attr('file_id');
			$("#tree").jstree("delete_node", file_id_);
			//$('#tree').jstree("refresh");
			var file_id = file_id_.substring(0, file_id_.lastIndexOf("/"));
			if(file_id == '/' || $.trim(file_id) == ''){
				$('#tree').jstree("refresh");
				return false;
			}
			setTimeout(function(){ 
				var res = file_id.split("/");
				$("#tree").jstree('open_node','/');
				var str = '';
				for(var i = 0; i< res.length; i++){
					str += (i == 0?'':'/') + res[i];
					$("#tree").jstree('open_node', str);
				}
				$("[id='"+file_id+"_anchor']").trigger('dblclick');
				$("[id='"+file_id+"_anchor']").trigger('click');
			}, 1000);
		}
	});
	
	$('#tree').on("select_node.jstree", function (e, data) {
		$("[id='"+data.node.id+"_anchor']").trigger('dblclick');
	});
	$(document).on('click', '.view', function(e, data){

	});
	$(document).on('click', '.create_new_folder', function(){
		var file_id = $(this).attr('file_id');
		if($.trim(file_id) == ''){
			file_id = "/";
			$("#tree").jstree("create_node", file_id);
			$('#tree').jstree("refresh", true, true);
			$('#tree').jstree("refresh");
			return false;
		}
		$("#tree").jstree("create_node", file_id);
			var res = file_id.split("/");
			$("#tree").jstree('open_node','/');
			var str = '';
			for(var i = 0; i< res.length; i++){
				str += (i == 0?'':'/') + res[i];
				$("#tree").jstree('open_node', str);
			}
			$("[id='"+file_id+"_anchor']").trigger('dblclick');
			$("[id='"+file_id+"_anchor']").trigger('click');
		return false;		
	});
	$(document).on('click', '.jstree-icon', function(e, data){
		id = opend_folder.node.id;
	});
	$(document).on('click', '#blueimp-gallery', function(){
		$(this).css('display', 'none');   
	})
	$(document).on('click', '.custom_view', function(){
		var file_id = $(this).attr('href');
		var file = '<?php echo Yii::app()->params['domain']; ?>/' + file_id;
		//var file = "http://ict.esterox.am/filestorage/home/Cohort2012/Course1/Semester1/Student Uploads/test.pdf";
		var html = '<iframe style="margin:0 auto;display:block;width:80%;height:100%;" src="https://docs.google.com/gview?url='+file+'&embedded=true#:0.page.20">';
		$('#blueimp-gallery').addClass('blueimp-gallery blueimp-gallery-single blueimp-gallery-display blueimp-gallery-left blueimp-gallery-right');
		$('#blueimp-gallery').css('display', 'block');
		$('#blueimp-gallery .slides').html(html);
		return false;		
	});
	function doSetTimeout(i) {
	  setTimeout(function() { 
		 str += (i == 0?'':'/') + res[i];
		 console.log(str);
		 $("#tree").jstree('open_node', str);
	  }, 10000);
	}
	$(document).on('change', '#inputImage', function(){
		
		if ($('#inputImage').val() == '') {
            return false;
        }
		if(opend_folder.selected.length){
			$('#file_id').val(opend_folder.selected);
			
		}
		
		$('#files_form').ajaxForm({
            target: '.images_upload_error',
            success: function() { },
            beforeSend: function() { 

			
			},
            uploadProgress: function(event, position,total, percentComplete){
				console.log(percentComplete);
		        if(percentComplete == 100) {
					$('#inputImage').val('');
					//$('.upload_btn_title').text('Upload Files...');
					//$('.btn_upl').addClass('btn-info disabled');
					/*var file_id = opend_folder.selected.length?opend_folder.selected[0]:false;
					
					if(!file_id){
						$('#tree').jstree("refresh");
						return false;
					}
						res = file_id.split("/");
						$("#tree").jstree('open_node','/');
						var str = '';
						function doSetTimeout(i, time) {
							setTimeout(function() { 
							console.log(i); 
							$("#tree").jstree('open_node', str);
							$("[id='"+file_id+"_anchor']").trigger('dblclick');
							$("[id='"+file_id+"_anchor']").trigger('click');
							}, time);
						}
						var time = 0;
						for(var i = 0; i< res.length; i++){
							str += (i == 0?'':'/') + res[i];
							doSetTimeout(i, time,str);
						}
						
						setTimeout(function() { 
						
							//console.log(file_id);
							//$('#tree').jstree("refresh_node", file_id);
						}, 1000);*/
/* 		       }else{
					
					console.log(percentComplete);
					$('.upload_btn_title').text('Uploading... (' + percentComplete+ '%)');
					
				}
            },
			complete: function(xhr) {
				var file_id = opend_folder.selected.length?opend_folder.selected[0]:false;
					
					if(!file_id){
						$('#tree').jstree("refresh");
						return false;
					}
						res = file_id.split("/");
						$("#tree").jstree('open_node','/');
						var str = '';
						function doSetTimeout(i, time,str) {
							setTimeout(function() { 
							console.log(i); 
							$("#tree").jstree('open_node', str);
							$("[id='"+file_id+"_anchor']").trigger('dblclick');
							$("[id='"+file_id+"_anchor']").trigger('click');
							}, time);
						}
						var time = 0;
						for(var i = 0; i< res.length; i++){
							str += (i == 0?'':'/') + res[i];
							time = 1000;
							doSetTimeout(i, time,str);
						}
			
				//$('.upload_btn_title').text(xhr.responseText);
			}
        }).submit();
	});
	
	A();
	});
	function A(){
	localStorage.setItem("jstree", '{"state":{"core":{"open":["/"],"scroll":{"left":0,"top":0},"selected":[]}},"ttl":false,"sec":1526839152223}');
	$('#tree')
		.jstree({
			'core' : {
				"open_parents": true,
				"load_open": true,
				'data' : {
					'url' : '?operation=get_node',
					'data' : function (node) {
						
						return { 'id' : node.id };
					}
				},
				'check_callback' : function(o, n, p, i, m) {
					if(m && m.dnd && m.pos !== 'i') { return false; }
					if(o === "move_node" || o === "copy_node") {
						if(this.get_node(n).parent === this.get_node(p).id) { return false; }
					}
					return true;
				},
				'themes' : {
					'responsive' : false,
					'variant' : 'small',
					'stripes' : true
				}
			},
			'search': {
                'show_only_matches': true,
                'fuzzy': false
            },
            'plugins': ['search', 'sort'],
			'sort' : function(a, b) {
				return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
			},
			'contextmenu' : {
				'items' : function(node) {
					var tmp = $.jstree.defaults.contextmenu.items();
					delete tmp.create.action;
					tmp.create.label = "New";
					tmp.create.submenu = {
						"create_folder" : {
							"separator_after"	: true,
							"label"				: "Folder",
							"action"			: function (data) {
								var inst = $.jstree.reference(data.reference),
									obj = inst.get_node(data.reference);
									
									console.log(inst);
									
								inst.create_node(obj, { type : "default" }, "last", function (new_node) {
									setTimeout(function () { inst.edit(new_node); },0);
								});
							}
						},
						"create_file" : {
							"label"				: "File",
							"action"			: function (data) {
								var inst = $.jstree.reference(data.reference),
									obj = inst.get_node(data.reference);
								inst.create_node(obj, { type : "file" }, "last", function (new_node) {
									setTimeout(function () { inst.edit(new_node); },0);
								});
							}
						}
					};
					if(this.get_type(node) === "file") {
						delete tmp.create;
					}
					return tmp;
				}
			},
			"multiple": false,
			'types' : {
				'default' : { 'icon' : 'folder' },
				'file' : { 'valid_children' : [], 'icon' : 'file' }
			},
			'unique' : {
				'duplicate' : function (name, counter) {
					return name + ' ' + counter;
				}
			},
			'plugins' : ['state','dnd','sort','types','contextmenu','unique']
		})
		.on('delete_node.jstree', function (e, data) {
			$.get('?operation=delete_node', { 'id' : data.node.id })
				.fail(function () {
					data.instance.refresh();
				});
			parent_node = data.node.parents[0];
			$("[id='"+parent_node+"_anchor']").trigger('click');
		})
		.on('create_node.jstree', function (e, data) {
			$.get('?operation=create_node', { 'type' : data.node.type, 'id' : data.node.parent, 'text' : data.node.text })
				.done(function (d) {
					data.instance.set_id(data.node, d.id);
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('rename_node.jstree', function (e, data) {
			console.log("rrrrrr");
			$.get('?operation=rename_node', { 'id' : data.node.id, 'text' : data.text })

				.done(function (d) {
					console.log(d);
					 data.instance.set_id(data.node, d.id);
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('move_node.jstree', function (e, data) {
			$.get('?operation=move_node', { 'id' : data.node.id, 'parent' : data.parent })
				.done(function (d) {
					data.instance.refresh();
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('copy_node.jstree', function (e, data) {
			$.get('?operation=copy_node', { 'id' : data.original.id, 'parent' : data.parent })
				.done(function (d) {
					data.instance.refresh();
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('changed.jstree', function (e, data) {
			opend_folder = data;
			
				
				
				
				if(true || data && data.selected && data.selected.length) {
				if(data.selected.length > 1){
					
				}
				$.get('?operation=get_content&id=' + data.selected.join(':'), function (d) {
					
					if(d && typeof d.type !== 'undefined') {
						$('#data .content').hide();
						switch(d.type) {
							case 'text':
							case 'txt':
							case 'md':
							case 'htaccess':
							case 'log':
							case 'sql':
							case 'php':
							case 'js':
							case 'json':
							case 'css':
							case 'html':
								$('#data .code').show();
								$('#code').val(d.content);
								break;
							case 'png':
							case 'jpg':
							case 'jpeg':
							case 'bmp':
							case 'gif':
								$('#data .image img').one('load', function () { $(this).css({'marginTop':'-' + $(this).height()/2 + 'px','marginLeft':'-' + $(this).width()/2 + 'px'}); }).attr('src',d.content);
								$('#data .image').show();
								break;
							default:
								var dir_contain = d.dir_contain;
			
								var file_icons = {
									'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
									'png':'<a><i class="fa fa-file-image-o"></i></a>',
									'gif':'<a><i class="fa fa-file-image-o"></i></a>',
									'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
									'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
									'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
									'doc': '<a><i class="fa fa-file-word-o"></i></a>',
									'docx': '<a><i class="fa fa-file-word-o"></i></a>',
									'txt': '<a><i class="fa fa-file-text"></i></a>',
									'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
									'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
									'default': '<a><i class="fa fa-file-o"></i></a>',
								};
								//alert(d.content);
								role_path = false;
								d_content = d.content;
								if(d.content != "")
								{
									var arr = d.content.split('/');
									if(arr[3] && arr[3] == 'Student Uploads'){
										role_path = true;
									}
								}
								var html = '<div class="row icons-box">';
									html += '<div class="infont col-lg-12" style="width:98%">';
										html += '<div class="ibox float-e-margins">';
										html += '<div class="ibox-content table-responsive">';
										html += '<ol class="breadcrumb">';
											html += '<li><a file_id="/" style="color:#1C84C6;text-decoration:underline;" class="open_dir">Home</a></li>';
											var brcs = (d.content).split("/");
											var str = ''; 
											if(brcs.length){
												for(var i = 0; i < brcs.length; i++){
													if(i == brcs.length - 1){
														html += '<li class="active"style="color:#1ab394"><strong>' + brcs[i] + '</strong></li>';
													}else{
														str += (i == 0?'':'/') + brcs[i];
														html += '<li><a style="color:#1C84C6;text-decoration:underline;" class="open_dir" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
													}
												}
											}
			
										html += '</ol>';
										if(user_role == 2 || user_role == 1 || role_path != false  )
										{
											html += '<a style="color:white; float: left;" file_id = "' + d.content + '" class="btn btn-primary create_new_folder" href="">Create New Folder</a>';
										}
										html += '<div style="float: left;">';
			
										
	
				
											if(user_role == 2 || user_role == 1 || role_path != false  )
											{
												html += '<form style="" id="files_form" method="post" action="" enctype="multipart/form-data">';
													html += '<label style="width:155px;margin-left:5px;" title=" Upload Files " for="inputImage" class="btn  btn_upl btn-primary">';
													html += '<input type="file" name="file[]" id="inputImage" class="hide" multiple="multiple">';
													html += '<span class="upload_btn_title">Upload Files</span>';
													html += '</label>';
													html += '<input type="hidden" id="file_id"  name="file_id" value="" />';
												html += '</form>';
											}
										html += '</div>';
								    html += '<table class="table table-hover files_datatable">';
									html += '<thead>';
										html += '<tr>';
											html += '<th>Type</th>';
											html += '<th>Name</th>';
											html += '<th>Download/View</th>';
											html += '<th>Remove</th>';
											html += '<th>Size</th>';
											html += '<th>Date</th>';
										html += '</tr>';
									html += '</thead>';
									html += '<tbody>';
									
										for(var i in dir_contain){
											var root = '';
											if(i == 0){
												root = 'root';
											}
											
											html += '<tr class="'+root+'">';
												if(dir_contain[i].type == 'folder'){
													html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
													html += '<td><input class="open_dir "  type_f="falder" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
													//html += '<input class="hidden  file_name" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
															
												//	html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
													html +=	'<td><a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="download_item fa fa-file-zip-o" file_id="'+dir_contain[i].file_id+'" ></i></a></td>';
													if(user_role == 2 || user_role == 1 || role_path != false  ){
														
														html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													}
													else
													{
														html += '<td></td>';
														
													}
													html += '<td></td>';
												}else{
													//file_icons
													
													var ext = (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1);
													var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
													
													html += '<td class="first_column"><span style="display:none;">'+ (dir_contain[i].name).substr((dir_contain[i].name).lastIndexOf('.') + 1)+'</span>' + file_icon + '</td>';
													html += '<td>';
														html += '<input class="f_name"  type_f="file" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" />';
													//	html += '<div class="div_rename" style="display: none; position: absolute" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'"><input type="text" class="input_rename" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'" value="'+dir_contain[i].name+'" /></div>';
													html += '</td>';
													html +=	'<td>';
														html +=	'<a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].file_id+'"><i class="fa fa-save download_item"></i></a>';
														var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
														var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
														html +=	'<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + dir_contain[i].file_id+'" class="' + data_gallery_class + ' view" file_id="'+dir_contain[i].file_id+'"><i class="view_item fa fa-eye"></i></a>';
													
													html +=	'</td>';
													if(user_role == 2 || user_role == 1 || role_path != false  ){
														html += '<td><a><i file_id="'+dir_contain[i].file_id+'" class="remove fa fa-times-circle"></i></a></td>';
													}
													else
													{
														html += '<td></td>';
													}
													html += '<td>'+dir_contain[i].size+'</td>';
												}
												html += '<td>'+dir_contain[i].date+'</td>';
											html += '</tr>';
											
										}
									html += '</tbody>';
									
								html += '</table>';
								html += '</div></div></div></div>';
						
								if(d.file_type != 'file'){
									$('#data .default').html(html).show();
									setTimeout(function(){
										if( (opend_folder.node && opend_folder.node.id) ){
											var tmp = opend_folder.node.id;
										}else{
											var tmp = '/';
										}
										$('#tree').jstree("refresh_node", tmp);
									}, 1000);
									
										
									$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
								}
								break;
						}
					}
				});
			}
			else {
				//$('#data .content').hide();
				//$('#data .default').html('').show();
			}
		}).on("refresh.jstree", function (event, data) {
			//
		});
		
		
		
		
		
		
		
		
		
		
		
		

	}
	
	$(document).on('click', '.file_name', function(){
		file_id = $(this).attr('file_id');
		
		$('a[file_id="'+ file_id +'"]').click();
	});
	
	$(document).on('contextmenu', '.open_dir, .f_name', function(){
		file_id = $(this).attr('file_id');
		var user_id = $(this).attr('user_id');
		var role_p_ = false;
		if(file_id != "")
		{
			var arr = file_id.split('/');
			if(arr[4] && arr[3] && arr[3] == 'Student Uploads'){
				role_p_ = true;
			}
		}
		if(user_role == 2 || user_role == 1 || role_p_ !=false ){
			//file_id = $(this).attr('file_id');
			
			$(this).addClass('file_name');
			$(this).removeClass('open_dir');
			$(this).removeAttr('readonly').css('border', '1px solid').css('background', 'white');
			
			return false;
		}
	});
	
	$(document).on('mouseover', '.table-hover tr', function(){
		$(this).find('.file_name').css('background', '#F5F5F5');
	});

	$(document).on('mouseleave', '.table-hover tr', function(){
		$(this).find('.file_name').css('background', 'white');
	});
	
	$(document).on('blur', '.file_name', function(e){
	
		var type_f = $(this).attr('type_f');
		if(type_f == 'falder'){
			$(this).addClass('open_dir');
		}
		
		$(this).attr('readonly', 'readonly').css('border', 'none').css('background', '#F5F5F5');
		//alert($(e.target).hasClass('open_dir'));
		$(this).removeClass('file_name');
		var file_id_ = $(this).attr('file_id');
		
		//alert(file_id_);
	//	var val = $('input[file_id="'+ file_id_ +'"]').val();
	//	console.log(file_id_);
		//if(!$(e.target).hasClass('open_dir')){
			$("#tree").jstree("rename_node", file_id_, $(this).val());
			console.log(file_id_);
		//}	
		//	$(this).css('display', 'none');
	});
	
	$(document).on('keyup', '#top-search', function(){
		var search_value = $(this).val();
		search_value = $.trim(search_value);
		
		var file_icons = {
							'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
							'png':'<a><i class="fa fa-file-image-o"></i></a>',
							'gif':'<a><i class="fa fa-file-image-o"></i></a>',
							'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
							'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
							'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
							'doc': '<a><i class="fa fa-file-word-o"></i></a>',
							'docx': '<a><i class="fa fa-file-word-o"></i></a>',
							'txt': '<a><i class="fa fa-file-text"></i></a>',
							'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
							'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
							'default': '<a><i class="fa fa-file-o"></i></a>',
						};
		
		var html = '<div class="row icons-box">';
				html += '<div class="infont col-lg-12" style="width:98%">';
					html += '<div class="ibox float-e-margins">';
					html += '<div class="ibox-content table-responsive">';
					html += '<ol class="breadcrumb">';
						html += '<li><a file_id="/" style="color:#1C84C6;text-decoration:underline;" class="open_dir">Home</a></li>';
											//var brcs = (d.content).split("/");
											/*var str = ''; 
											if(brcs.length){
												for(var i = 0; i < brcs.length; i++){
													if(i == brcs.length - 1){
														html += '<li class="active"style="color:#1ab394"><strong>' + brcs[i] + '</strong></li>';
													}else{
														str += (i == 0?'':'/') + brcs[i];
														html += '<li><a style="color:#1C84C6;text-decoration:underline;" class="open_dir" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
													}
												}
											}*/
			
/*					html += '</ol>';
					
					if(user_role == 2 || user_role == 1 || role_path != false  )
					{
						html += '<a style="color:white; float: left;" file_id = "' + d_content + '" class="btn btn-primary create_new_folder" href="">Create New Folder</a>';
					}
					
					if(user_role == 2 || user_role == 1 || role_path != false  )
					{
						html += '<div style="float: left;">';
						html += '<form style="" id="files_form" method="post" action="" enctype="multipart/form-data">';
							html += '<label style="width:155px;margin-left:5px;" title=" Upload Files " for="inputImage" class="btn btn-primary">';
							html += '<input type="file" name="file[]" id="inputImage" class="hide" multiple="multiple">';
							html += '<span class="upload_btn_title">Upload Files</span>';
							html += '</label>';
							html += '<input type="hidden" id="file_id"  name="file_id" value="" />';
						html += '</form>';

					html += '</div>';
					}
						 html += '<table class="table table-hover files_datatable">';
							html += '<thead>';
								html += '<tr>';
									html += '<th>Type</th>';
									html += '<th>Name</th>';
									html += '<td>Location</td>';
									html += '<th>Download/View</th>';
									html += '<th>Remove</th>';
									html += '<th>Size</th>';
									html += '<th>Date</th>';
								html += '</tr>';
							html += '</thead>';
							html += '<tbody>';
		if(search_value != ""){
			$.ajax({
				url:'http://www.unasat.org/ict_new/',
				type:'POST',
				data:{search:search_value},
				success:function(data){
					var d = $.parseJSON(data);
					//alert(d[0].type);
					if(d == "")
					{
						html +='<tr><td>No matching records found</td></tr></tbody>';
						$('#data .default').html(html).show();
					
					}
					else
					{
						for(var i in d){
							
							if(d[i].type == 'folder'){
								html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
								html += '<td><input class="open_dir " type_f="falder" user_id="'+d[i].user_id+'" file_id="'+ d[i].path +'" oncontextmenu="return false;" value="'+ d[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
								html += '<td>'+ d[i].path +'</td>';
								//	html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
								html +=	'<td><a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+d[i].path+'"><i class="download_item fa fa-file-zip-o" file_id="'+d[i].path+'" ></i></a></td>';
								if(user_role == 2 || user_role == 1 || user_get_id == d[i].user_id ){
									html += '<td><a><i file_id="'+d[i].path+'" class="remove fa fa-times-circle"></i></a></td>';
								}else{
									html += '<td></td>';
								}
								html += '<td></td>';
								
								html += '<td>"'+d[i].date+'"</td>';
							}
							else
							{
								var ext = d[i].type;
								var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
								html += '<td class="first_column"><span style="display:none;">'+ d[i].type+'</span>' + file_icon + '</td>';
								html += '<td>';
									html += '<input class="f_name" file_id="'+ d[i].path +'" oncontextmenu="return false;" user_id="'+d[i].user_id+'" value="'+ d[i].name +'" style="border: none; outline: none" readonly="radonly" />';
									
								//	html += '<div class="div_rename" style="display: none; position: absolute" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'"><input type="text" class="input_rename" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'" value="'+dir_contain[i].name+'" /></div>';
								html += '</td>';
								html += '<td>'+ d[i].path +'</td>';
								html +=	'<td>';
									html +=	'<a href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+d[i].path+'"><i class="fa fa-save download_item"></i></a>';
										var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
										var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
									html +=	'<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + d[i].path+'" class="' + data_gallery_class + ' view" file_id="'+d[i].path+'"><i class="view_item fa fa-eye"></i></a>';
														
								html +=	'</td>';
								if(user_role == 2 || user_role == 1 || user_get_id == d[i].user_id  ){
									html += '<td><a><i file_id="'+d[i].path+'" class="remove fa fa-times-circle"></i></a></td>';
								}else{
									html += '<td></td>';
								}
								html += '<td>'+d[i].size+'</td>';
								html += '<td>'+d[i].date+'</td>';
							}
								//html += '<td>'+d[i].date+'</td>';
							html += '</tr>';
						}
											/*for(var i in dir_contain){
												var root = '';
												if(i == 0){
													root = 'root';
												}
												*/
/*										html += '</tbody>';
										
									html += '</table>';
									html += '</div></div></div>';
						$('#data .default').html(html).show();
						$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
						
						
					}
				},
				error:function(){alert("error");}  

			});
			
			//alert(search_value);
		}
		else
		{
			html +='<tr><td>No matching records found</td></tr></tbody>';
			$('#data .default').html(html).show();
			/*$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false
										//bFilter:false
										
									});*/
		
/*		}
		
		
	});
	*/

	
	
</script>
