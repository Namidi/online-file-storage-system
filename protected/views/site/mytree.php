<?php
//chante isStudent to isAdmin
if(Yii::app()->user->isStudent()) {
	$hidden = 'hidden';
	$right_box = 'style="margin-left: 0px;"';
} else {
	$hidden = '';
	$right_box = '';
}
//var_dump(Yii::app()->params['domain']);
?>

<div id="container" role="main">

	<div class="left_side hidden <?php echo $hidden; ?>">
		<!--<div class="infont col-md-3 col-sm-4"><a href="#"><i class="fa fa-times-circle"></i></a></div>-->
		<!--
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-bottom:10px;"></div>
		<span style="color:red;" class="images_upload_error"></span>
		<form id="files_form" method="post" action="" enctype="multipart/form-data">
			<label style="width:276px;margin-left:20px;" title=" Upload Files " for="inputImage" class="btn btn-primary">
				<input type="file"  name="file[]" id="inputImage" class="hide" multiple="multiple">
				<span class="upload_btn_title">Upload Files</span>
			</label>
			<input type="hidden" id="file_id"  name="file_id" value="" />
		</form>
		-->
		<div style="margin-left:20px;width:276px;height:1px;border-bottom:1px dotted #1ab394;margin-top:10px;"></div>
		<div id="tree"></div>
	</div>
	<div id="data" style="margin-left: 0px;"  <?php //echo $right_box; ?> >
		<div class="content code" style="display:none;"><textarea id="code" readonly="readonly"></textarea></div>
		<div class="content folder" style="display:none;"></div>
		<div class="content image" style="display:none; position:relative;"><img src="" alt="" style="display:block; position:absolute; left:50%; top:50%; padding:0; max-height:90%; max-width:90%;" /></div>
		<div class="default" style="">
			<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />
		</div>
	</div>
</div>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.fileupload.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/jstree/js/jstree.js"></script> -->

<style>
.hover-name:hover{
	color:#1C84C6;
	text-decoration:underline;
	cursor:pointer;
.open_dir_li{
	

}	
}

</style>

<script>

window.user_uploads = "<?php echo $user_uploads; ?>";
window.user_get_id = "<?php echo Yii::app()->user->getId(); ?>";
window.user_role = "<?php echo $user_role; ?>";
window.open_docx_folder_path = "<?php echo Yii::app()->params['domain']. DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR .'home' . DIRECTORY_SEPARATOR; ?>";
window.base_folder_path = '<?php echo Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'filestorage' . DIRECTORY_SEPARATOR .'home' . DIRECTORY_SEPARATOR; ?>';
window.loader = '<img src="<?php echo Yii::app()->baseUrl; ?>/images/loading16.gif" width="25" />';
$(document).ready(function(){ 
	get_folder_contine('/','home');
	
	
	function getFileSize(size){
		//var size = filesize($path);
		var unit = 'B';
		
		if(size > 1024){
			size = size / 1024;
			unit = 'KB';
			if(size > 1000) {
				size = size / 1024;
				unit = 'MB';
				if(size > 1000) {
					size = size / 1024;
					unit = 'GB';
				}
			}
		}
		var res = (Math.round(size*100)/100) +' '+ unit;
		
		return res;
	}
	
	
	
	
	
	
	function get_folder_contine(id,post_name){
		$.ajax({
				url:'<?php echo Yii::app()->baseUrl; ?>'+'/',
				type:'POST',
				//dataType:'json',
				data:{id:id,'action':post_name},
				
				success:function(data){
					var dir_contain = $.parseJSON(data);
					//console.log(d[0].path);
					var file_icons = {
									'pptx':'<a><i class="fa fa-file-powerpoint-o"></i></a>',
									'ppt':'<a><i class="fa fa-file-powerpoint-o"></i></a>',
									'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
									'png':'<a><i class="fa fa-file-image-o"></i></a>',
									'gif':'<a><i class="fa fa-file-image-o"></i></a>',
									'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
									'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
									'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
									'doc': '<a><i class="fa fa-file-word-o"></i></a>',
									'docx': '<a><i class="fa fa-file-word-o"></i></a>',
									'txt': '<a><i class="fa fa-file-text"></i></a>',
									'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
									'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
									'default': '<a><i class="fa fa-file-o"></i></a>',
								};
								//alert(d.content);
								role_path = false;
								if(dir_contain != null){
									d_content = dir_contain.path;
								}	
									if(id != "")
									{
										var arr = id.split('/');
										for(var i=0;i<arr.length;i++){
											if(arr[i] == 'Student Uploads'){
										
												role_path = true;
												break;
											}
										}
									
									}
								
								var html = '<div class="row icons-box">';
									html += '<div class="infont col-lg-12" style="width:98%">';
										html += '<div class="ibox float-e-margins">';
										html += '<div class="ibox-content table-responsive">';
										if(user_role == 2 || user_role == 1 || role_path != false  )
										{
											html += '<a style="color:white; " file_id = "' + id + '" class="btn btn-primary create_new_folder" href="">Create New Folder</a>';
										}
										html += '<div class="div-uploads" style="display:inline-block;">';
																
											if(user_role == 2 || user_role == 1 || role_path != false  )
											{
												html += '<form style="" id="files_form" method="post" action="" enctype="multipart/form-data">';
													html += '<label style="width:155px;margin-left:5px;" title=" Upload Files " for="inputImage" class="btn  btn_upl btn-primary">';
													html += '<input type="file" name="file[]" id="inputImage" class="hide" multiple="multiple">';
													html += '<span class="upload_btn_title">Upload Files</span>';
													html += '</label>';
													html += '<input type="hidden" id="file_id"  name="file_id" value="'+id +'" />';
												html += '</form>';
												html += '<div class="error"></div>';
											}
										html += '</div>';
										
										html += '<ol class="breadcrumb">';
											html += '<li><a file_id="/" style="color:#1C84C6;text-decoration:underline; font-size:14px" class="open_dir_li">Home</a></li>';
												if(id != '/'){
													var brcs = id.split("/");
													var str = ''; 
													if(brcs.length){
														for(var i = 0; i < brcs.length; i++){
															if(i == brcs.length - 1){
																html += '<li class="active"style="color:#1ab394; font-size:14px"><strong>' + brcs[i] + '</strong></li>';
															}else{
																str += (i == 0?'':'/') + brcs[i];
																html += '<li><a style="color:#1C84C6;text-decoration:underline; font-size:14px" class="open_dir_li" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
															}
														}
													}
												}
										html += '</ol>';
										
										
										
								    html += '<table class="table table-hover files_datatable">';
									html += '<thead>';
										html += '<tr>';
											html += '<th>Type</th>';
											html += '<th>Name</th>';
											html += '<th>Download/View</th>';
											html += '<th>Remove</th>';
											html += '<th>Size</th>';
											html += '<th>Date</th>';
										html += '</tr>';
									html += '</thead>';
									html += '<tbody>';
									
									if(dir_contain != null){
										//console.log(dir_contain);
											for(var i in dir_contain){
												var root = '';
												var name = dir_contain[i].path.substring(dir_contain[i].path.lastIndexOf('/') + 1);
												if(dir_contain[i].tupe != 0){
													var size = getFileSize(dir_contain[i].size);
												}else{
													var size = "";
												}
												//alert(name);
												if(i == 0){
													root = 'root';
												}
												
												html += '<tr class="'+root+'">';
													if(dir_contain[i].type == 0){
														html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
														html += '<td><input class="open_dir hover-name " user_id="'+dir_contain[i].user_id+'" type_f="falder" file_id="'+ dir_contain[i].path +'" oncontextmenu="return false;" value="'+ name+'" style="border: none; outline: none" readonly="radonly" /></td>';
														//html += '<input class="hidden  file_name" file_id="'+ dir_contain[i].file_id +'" oncontextmenu="return false;" value="'+ dir_contain[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
																
													//	html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
														if( user_role == 1 || user_role == 2 || dir_contain[i].user_id == user_get_id  ){
															
															html +=	'<td><a class="rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].path+'"><i class="download_item  rep-file-id   fa fa-file-zip-o" file_id="'+dir_contain[i].path+'" ></i></a></td>';
														}else{
																html +=	'<td></td>';
														}
														if( user_role == 1 || dir_contain[i].user_id == user_get_id  ){
															
															html += '<td><a><i file_id="'+dir_contain[i].path+'" class="remove rep-file-id fa fa-times-circle"></i></a></td>';
														}
														else
														{
															html += '<td></td>';
															
														}
														html += '<td></td>';
													}else{
														//file_icons
														
														var ext = (dir_contain[i].path).substr((dir_contain[i].path).lastIndexOf('.') + 1).toLowerCase();
														var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
														
														html += '<td class="first_column"><span style="display:none;">'+ (dir_contain[i].path).substr((dir_contain[i].path).lastIndexOf('.') + 1)+'</span>' + file_icon + '</td>';
														html += '<td>';
														if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf' || ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
															html += '<input class="f_name hover-name"  type_f="file" user_id="'+dir_contain[i].user_id+'" file_id="'+ dir_contain[i].path +'" oncontextmenu="return false;" value="'+ name +'" style="border: none; outline: none" readonly="radonly" />';
														}else{
															html += '<input class="f_name "  type_f="file" user_id="'+dir_contain[i].user_id+'" file_id="'+ dir_contain[i].path +'" oncontextmenu="return false;" value="'+ name +'" style="border: none; outline: none" readonly="radonly" />';
															
														}	
														//	html += '<div class="div_rename" style="display: none; position: absolute" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'"><input type="text" class="input_rename" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'" value="'+dir_contain[i].name+'" /></div>';
														html += '</td>';
														html +=	'<td>';
															html +=	'<a class="rep-file-id rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+dir_contain[i].url+'"><i class="fa fa-save  download_item"></i></a>';
															var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
															var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
															if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf'){
																html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + dir_contain[i].path+'" class="' + data_gallery_class + ' view  rep-href-view rep-file-id" file_id="'+dir_contain[i].path+'">';
																	html += '<i class="view_item fa fa-eye"></i>';
																html +=	'</a>';
																														
															}else if(ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
																html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="http://view.officeapps.live.com/op/view.aspx?src='+open_docx_folder_path + dir_contain[i].url+'" class="' + data_gallery_class + ' view rep-href-view  rep-file-id" file_id="'+dir_contain[i].path+'">';
																//if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf'){
																	html += '<i class="view_item fa fa-eye"></i>';
																//}
																html +=	'</a>';
															}	
														html +=	'</td>';
														if(dir_contain[i].user_id == user_get_id || user_role == 1   ){
															html += '<td><a><i file_id="'+dir_contain[i].path+'" class="remove  rep-file-id fa fa-times-circle"></i></a></td>';
														}
														else
														{
															html += '<td></td>';
														}
														html += '<td>'+size+'</td>';
													}
													html += '<td>'+dir_contain[i].date+'</td>';
												html += '</tr>';
												
											}
										html += '</tbody>';
										
										html += '</table>';
										html += '</div></div></div></div>';
										$('#data .default').html(html).show();
									}else{
										html +='<tr class="not-found"><td></td>';
										html +='<td>This folder is empty</td>';
										html +='<td></td>';
										html +='<td></td>';
										html +='<td></td>';
										html +='<td></td>';
										html +='</tr></tbody>';
										html += '</table>';
										html += '</div></div></div></div>';
										$('#data .default').html(html).show();
									
									}	
								/*if(d.file_type != 'file'){
									
									setTimeout(function(){
										if( (opend_folder.node && opend_folder.node.id) ){
											var tmp = opend_folder.node.id;
										}else{
											var tmp = '/';
										}
										$('#tree').jstree("refresh_node", tmp);
									}, 1000);
									
										
									$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
								}
								*/
							$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
					
					
				},
				error:function(){ alert("error"); }  

		});


	}


	$(document).on('click', '.create_new_folder', function(){
			var file_id_ = $(this).attr('file_id');
			$.ajax({
				url:'<?php echo Yii::app()->baseUrl; ?>'+'/',
				type:'POST',
				//dataType:'json',
				data:{id:file_id_,'action':'create_new_folder'},
				
				success:function(data){
					var result = $.parseJSON(data);
					if(result !== false){
						
						//$_this.parents('tr').addClass('hidden');
						var name = result.path.substring(result.path.lastIndexOf('/') + 1);
						var html = "";
						$('.not-found').remove();
						html += '<tr class="root">';
						html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
						html += '<td><input class="open_dir hover-name" user_id="'+result.user_id+'" type_f="falder" file_id="'+ result.path +'" oncontextmenu="return false;" value="'+ name+'" style="border: none; outline: none" readonly="radonly" /></td>';
						html +=	'<td><a class="rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+result.path+'"><i class="download_item fa fa-file-zip-o" file_id="'+result.path+'" ></i></a></td>';
						html += '<td><a><i file_id="'+result.path+'" class="remove rep-file-id fa fa-times-circle"></i></a></td>';
						html += '<td></td>';
						html += '<td>'+result.date+'</td>';
						html += '</tr>';
						$('.table').append(html);
					}
				},
				error:function(){alert(errorrr);}
			});	
				
				return false;
			
			
			
	});

	$(document).on('click', '.remove', function(){
		$_this = $(this);
		result = confirm('Do you really want to delete this item?');
		
		if(result == true){
			var file_id_ = $(this).attr('file_id');
			$.ajax({
				url:'<?php echo Yii::app()->baseUrl; ?>'+'/',
				type:'POST',
				//dataType:'json',
				data:{id:file_id_,'action':'remove'},
				
				success:function(data){
					var result = $.parseJSON(data);
					if(result === true){
						$_this.parents('tr').addClass('hidden');
						
					}
				},
				error:function(){alert(errorrr);}
			});	
		}
	});
	
	$(document).on('contextmenu', '.open_dir, .f_name', function(){
		
		//$(this).css({'cursor':'auto'});
		file_id = $(this).attr('file_id');
		var user_id = $(this).attr('user_id');
		//var role_p_ = false;
		/*if(file_id != "")
		{
			var arr = file_id.split('/');
			//var n_arr = arr[arr.length-1];
			for(var i=0; i<arr.length;i++){
				if( arr[i] == 'Student Uploads' && user_id == user_get_id){
					role_p_ = true;
					break;
				}
			}
		}*/
		if( user_role == 1 || user_id == user_get_id ){
			//file_id = $(this).attr('file_id');
			$(this).removeClass('hover-name');
			$(this).addClass('file_name');
			$(this).removeClass('open_dir');
			$(this).removeClass('f_name');
			$(this).removeAttr('readonly').css('border', '1px solid').css('background', 'white');
			text = $(this).val();
			var res = text.split('.');
			f_type = res[text.split('.').length-1];
			return false;
		}
	});
	
	$(document).on('blur', '.file_name', function(e){
		
		$_this_blur = $(this);
		var type_f = $(this).attr('type_f');
		if(type_f == 'falder'){
			$(this).addClass('open_dir');
			$(this).addClass('hover-name');
		}else{
			$(this).addClass('f_name');
		}
		$(this).attr('readonly', 'readonly').css('border', 'none').css('background', '#F5F5F5');
		$(this).removeClass('file_name');
		var th_text = $(this).val();
		var res = th_text.split('.');
		var th_type = res[th_text.split('.').length-1];
		
		
		
		if(f_type != th_type && type_f != 'falder'){
			if(f_type == 'jpg' || f_type == 'jpeg' || f_type == 'png' || f_type == 'gif' || f_type == 'pdf' || f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
				$_this_blur.addClass('hover-name');
			}
			$(this).val(text);
		}else if(text != th_text){
			var file_id_ = $(this).attr('file_id');
			$.ajax({
				url:'<?php echo Yii::app()->baseUrl; ?>'+'/',
				type:'POST',
				data:{id:file_id_,'action':'rename','name':th_text},
				success:function(data){
					var result = $.parseJSON(data);
					
					if(result !== null){
						$(this).val(th_text);
						if(f_type == 'jpg' || f_type == 'jpeg' || f_type == 'png' || f_type == 'gif' || f_type == 'pdf' || f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
							$_this_blur.addClass('f_name');
							$_this_blur.addClass('hover-name');
						}
						
						$_this_blur.attr('file_id',result.res_id);
						$_this_blur.parents('tr').find('.rep-file-id').attr('file_id',result.res_id);
						$_this_blur.parents('tr').find('.rep-href-dow').attr('href','<?php echo Yii::app()->createUrl('site/download'); ?>'+'?id='+result.res);
						if( f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
							
							
							$_this_blur.parents('tr').find('.rep-href-view').attr('href',"http://view.officeapps.live.com/op/view.aspx?src="+open_docx_folder_path + result.res);
						}else{
							
							
							$_this_blur.parents('tr').find('.rep-href-view').attr('href',base_folder_path + result.res_id);
						}
					}else if(text == th_text ){
						if(f_type == 'jpg' || f_type == 'jpeg' || f_type == 'png' || f_type == 'gif' || f_type == 'pdf' || f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
							$_this_blur.addClass('hover-name');
							$_this_blur.addClass('f_name');
						}
						
						$_this_blur.val(text);
						
					}else{
						if(f_type == 'jpg' || f_type == 'jpeg' || f_type == 'png' || f_type == 'gif' || f_type == 'pdf' || f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
							$_this_blur.addClass('hover-name');
							$_this_blur.addClass('f_name');
						}

						$_this_blur.val(th_text);
					}
				},
				error:function(){alert(errorrr);}
			});	
		}else{
			
			if(f_type == 'jpg' || f_type == 'jpeg' || f_type == 'png' || f_type == 'gif' || f_type == 'pdf' || f_type == 'pptx' || f_type == 'ppt' || f_type == 'xlsx' || f_type == 'xls' || f_type == 'doc' || f_type == 'docx' ){
				$_this_blur.addClass('hover-name');
				$_this_blur.addClass('f_name');
			}
		}
	});
	
	
	
	
	$(document).on('mouseover', '.table-hover tr', function(){
		
		$(this).find('.f_name,  .open_dir').css('background', '#F5F5F5');
	});
	$(document).on('mouseover', '.open_dir , .f_name', function(){
		
		//$(this).css({'cursor':'pointer'});
		
	});
	

	$(document).on('mouseleave', '.table-hover tr', function(){
		$(this).find('.f_name, .open_dir').css('background', 'white');
	});
	
	$(document).on('click', '.f_name', function(){
		file_id = $(this).attr('file_id');
		var ext = (file_id).substr((file_id).lastIndexOf('.') + 1).toLowerCase();
		//console.log(ext);
		if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif'  ){
			$('a[file_id="'+ file_id +'"]').click();
			
		}else if(ext == 'pdf' || ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx' ){
			var href = $('a[file_id="'+ file_id +'"]').attr('href');
			window.open(href,'_blank');
		}	
	});
	
	
	
	$(document).on('click','.open_dir', function(){
		var file_id = $(this).attr('file_id');
		get_folder_contine(file_id,'open_dir');
		
		
		
		return false;
	});
	$(document).on('click','.open_dir_li', function(){
		var file_id = $(this).attr('file_id');
		get_folder_contine(file_id,'open_dir');
		
		
		
		return false;
	});
	
	
	$(document).on('change', '#inputImage', function(){
		
		if ($('#inputImage').val() == '') {
            return false;
        }
		/*if(opend_folder.selected.length){
			$('#file_id').val(opend_folder.selected);
			
		}*/
		
		$('#files_form').ajaxForm({
            target: '.images_upload_error',
            success: function(data) { 
				//console.log(data);
				
			
			},
            beforeSend: function() { 

			
			},
            uploadProgress: function(event, position,total, percentComplete){
				//console.log(percentComplete);
		        if(percentComplete == 100) {
					
					$('#inputImage').val('');
					
					
		       }else{
					
					//console.log(percentComplete);
					$('.upload_btn_title').text('Uploading... (' + percentComplete+ '%)');
					
				}
            },
			complete: function(xhr) {
				//console.log(xhr.responseText);
				$('.not-found').addClass('hidden');
					var result = $.parseJSON(xhr.responseText);
					if(result !== false && result != 'error'){
						var file_icons = {
									'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
									'png':'<a><i class="fa fa-file-image-o"></i></a>',
									'gif':'<a><i class="fa fa-file-image-o"></i></a>',
									'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
									'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
									'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
									'doc': '<a><i class="fa fa-file-word-o"></i></a>',
									'docx': '<a><i class="fa fa-file-word-o"></i></a>',
									'txt': '<a><i class="fa fa-file-text"></i></a>',
									'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
									'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
									'default': '<a><i class="fa fa-file-o"></i></a>',
								};
						
						$('.not-found').remove();
						for(var i in result){
							var html = "";
							var ext = result[i].ext.toLowerCase();
							var size = getFileSize(result[i].size); 
							var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
							html += '<tr>';
							html += '<td class="first_column"><span style="display:none;">'+ (result[i].path).substr((result[i].path).lastIndexOf('.') + 1)+'</span>' + file_icon + '</td>';
							html += '<td>';
							if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf' || ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
								html += '<input class="f_name hover-name"  type_f="file" user_id="'+result[i].user_id+'" file_id="'+ result[i].path +'" oncontextmenu="return false;" value="'+ result[i].name +'" style="border: none; outline: none" readonly="radonly" />';
							}else{
								html += '<input class="f_name"  type_f="file" user_id="'+result[i].user_id+'" file_id="'+ result[i].path +'" oncontextmenu="return false;" value="'+ result[i].name +'" style="border: none; outline: none" readonly="radonly" />';
							}
							html += '</td>';
							html +=	'<td>';
								html +=	'<a class="rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+result[i].url+'"><i class="fa fa-save download_item"></i></a>';
								var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
								var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
								if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf'){
										html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + result[i].path+'" class="' + data_gallery_class + ' view  rep-href-view rep-file-id" file_id="'+result[i].path+'">';
											html += '<i class="view_item fa fa-eye"></i>';
										html +=	'</a>';
																														
								}else if(ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
										html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="http://view.officeapps.live.com/op/view.aspx?src='+open_docx_folder_path + result[i].url+'" class="' + data_gallery_class + ' view rep-href-view  rep-file-id" file_id="'+result[i].path+'">';
															
											html += '<i class="view_item fa fa-eye"></i>';
																
										html +=	'</a>';
								}
							html +=	'</td>';
							html += '<td><a><i file_id="'+result[i].path+'" class="remove rep-file-id  fa fa-times-circle"></i></a></td>';
							html += '<td>'+size+'</td>';
						
							html += '<td>'+result[i].date+'</td>';
							html += '</tr>';
							//$('.uploadded-size').text(result[i].tottal_size+'MB');
							$('.table').append(html);
						}
					}else if(result == 'error'){
						$('.error').text('Yor file  size limit full! ');
						$('.error').css({'color':'#FF0000'});
						$('.error').show();
						setTimeout(function() { 
							$('.error').hide();
						}, 4000);
					}
				$('.upload_btn_title').text('Upload Files');
				
			}
        }).submit();
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$(document).on('keyup', '#top-search', function(){
		var search_value = $(this).val();
		search_value = $.trim(search_value);
		
		
		var file_icons = {
							'pptx':'<a><i class="fa fa-file-powerpoint-o"></i></a>',
							'ppt':'<a><i class="fa fa-file-powerpoint-o"></i></a>',
							'jpg':'<a><i class="fa fa-file-image-o"></i></a>',
							'png':'<a><i class="fa fa-file-image-o"></i></a>',
							'gif':'<a><i class="fa fa-file-image-o"></i></a>',
							'jpeg':'<a><i class="fa fa-file-image-o"></i></a>',
							'bmp':'<a><i class="fa fa-file-image-o"></i></a>',
							'pdf': '<a><i class="fa fa-file-pdf-o"></i></a>',
							'doc': '<a><i class="fa fa-file-word-o"></i></a>',
							'docx': '<a><i class="fa fa-file-word-o"></i></a>',
							'txt': '<a><i class="fa fa-file-text"></i></a>',
							'xls': '<a><i class="fa fa-file-excel-o"></i></a>',
							'xlsx': '<a><i class="fa fa-file-excel-o"></i></a>',
							'default': '<a><i class="fa fa-file-o"></i></a>',
						};
		
		var html = '<div class="row icons-box">';
				html += '<div class="infont col-lg-12" style="width:98%">';
					html += '<div class="ibox float-e-margins">';
					html += '<div class="ibox-content table-responsive">';
					html += '<ol class="breadcrumb">';
						html += '<li><a file_id="/" style="color:#1C84C6;text-decoration:underline; font-size:14px;" class="open_dir_li">Home</a></li>';
											//var brcs = (d.content).split("/");
											/*var str = ''; 
											if(brcs.length){
												for(var i = 0; i < brcs.length; i++){
													if(i == brcs.length - 1){
														html += '<li class="active"style="color:#1ab394"><strong>' + brcs[i] + '</strong></li>';
													}else{
														str += (i == 0?'':'/') + brcs[i];
														html += '<li><a style="color:#1C84C6;text-decoration:underline;" class="open_dir" file_id="' + str + '" href="">' + brcs[i] + '</a></li>';
													}
												}
											}*/
			
				html += '</ol>';
					
					
					
					
						 html += '<table class="table table-hover files_datatable">';
							html += '<thead>';
								html += '<tr>';
									html += '<th>Type</th>';
									html += '<th>Name</th>';
									html += '<th>Location</th>';
									html += '<th>Download/View</th>';
									html += '<th>Remove</th>';
									html += '<th>Size</th>';
									html += '<th>Date</th>';
								html += '</tr>';
							html += '</thead>';
							html += '<tbody>';
		if(search_value != ""){
			$.ajax({
				url:'<?php echo Yii::app()->baseUrl; ?>'+'/',
				type:'POST',
				data:{search:search_value},
				success:function(data){
					var d = $.parseJSON(data);
					//alert(d[0].type);
					if(d == "")
					{
						html += '<tr>';
							
						html +='<td>No matching records found</td>';
									html += '<td></td>';
									html += '<td></td>';
									html += '<td></td>';
									html += '<td></td>';
									html += '<td></td>';
									html += '<td></td>';
								html += '</tr>';
						$('#data .default').html(html).show();
						$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
					
					}
					else
					{
						
						for(var i in d){
							if(d[i].type != 'folder'){
								var size = getFileSize(d[i].size);
							}else{
								var size = "";
							}
							
							if(d[i].type == 'folder'){
								html += '<td class="first_column"><span style="display:none;">1</span><a><i class="fa fa-folder-o"></i></a></td>';
								html += '<td><input class="open_dir  hover-name " type_f="falder" user_id="'+d[i].user_id+'" file_id="'+ d[i].path +'" oncontextmenu="return false;" value="'+ d[i].name +'" style="border: none; outline: none" readonly="radonly" /></td>';
								html += '<td>'+ d[i].path +'</td>';
								//	html += '<td><a class="open_dir" file_id="'+dir_contain[i].file_id+'" href="">'+dir_contain[i].name+'</a></td>';
								if( user_role == 1 || user_role == 2 || d[i].user_id == user_get_id  ){
									html +=	'<td><a class="rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+d[i].path+'"><i class="download_item rep-file-id fa fa-file-zip-o" file_id="'+d[i].path+'" ></i></a></td>';
								}else{
									html +=	'<td></td>';
								}
								if( user_role == 1 || user_get_id == d[i].user_id ){
									html += '<td><a><i file_id="'+d[i].path+'" class="remove fa  rep-file-id fa-times-circle"></i></a></td>';
								}else{
									html += '<td></td>';
								}
								html += '<td></td>';
								
								html += '<td>'+d[i].date+'</td>';
							}
							else
							{
								var ext = d[i].type.toLowerCase();
								var file_icon = file_icons[ext]?file_icons[ext]:file_icons['default'];
								html += '<td class="first_column"><span style="display:none;">'+ d[i].type+'</span>' + file_icon + '</td>';
								html += '<td>';
								if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf' || ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
									html += '<input class="f_name hover-name" user_id="'+ d[i].user_id +'" file_id="'+ d[i].path +'" oncontextmenu="return false;" user_id="'+d[i].user_id+'" value="'+ d[i].name +'" style="border: none; outline: none" readonly="radonly" />';
								}else{
									html += '<input class="f_name" user_id="'+ d[i].user_id +'" file_id="'+ d[i].path +'" oncontextmenu="return false;" user_id="'+d[i].user_id+'" value="'+ d[i].name +'" style="border: none; outline: none" readonly="radonly" />';
			
								}	
								//	html += '<div class="div_rename" style="display: none; position: absolute" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'"><input type="text" class="input_rename" id="file_'+ i +'" file_id="'+dir_contain[i].file_id+'" value="'+dir_contain[i].name+'" /></div>';
								html += '</td>';
								html += '<td>'+ d[i].path +'</td>';
								html +=	'<td>';
									html +=	'<a class="rep-href-dow" href="<?php echo Yii::app()->createUrl('site/download'); ?>?id='+d[i].url+'"><i class="fa fa-save download_item"></i></a>';
										var data_gallery = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'data-gallery=""':'';
										var data_gallery_class = (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif')?'':'custom_view';
									if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'pdf'){
										html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="'+base_folder_path + d[i].path+'" class="' + data_gallery_class + ' view  rep-href-view rep-file-id" file_id="'+d[i].path+'">';
											html += '<i class="view_item fa fa-eye"></i>';
										html +=	'</a>';
																														
									}else if(ext == 'pptx' || ext == 'ppt' || ext == 'xlsx' || ext == 'xls' || ext == 'doc' || ext == 'docx'){
										html +=	'&nbsp;&nbsp;&nbsp;&nbsp;<a ' + data_gallery + ' target="_blank" href="http://view.officeapps.live.com/op/view.aspx?src='+open_docx_folder_path + d[i].url+'" class="' + data_gallery_class + ' view rep-href-view  rep-file-id" file_id="'+d[i].path+'">';
															
											html += '<i class="view_item fa fa-eye"></i>';
																
										html +=	'</a>';
									}		
													
								html +=	'</td>';
								if( user_role == 1 || user_get_id == d[i].user_id  ){
									html += '<td><a><i file_id="'+d[i].path+'" class="remove rep-file-id fa fa-times-circle"></i></a></td>';
								}else{
									html += '<td></td>';
								}
								html += '<td>'+size+'</td>';
								html += '<td>'+d[i].date+'</td>';
							}
								//html += '<td>'+d[i].date+'</td>';
							html += '</tr>';
						}
											/*for(var i in dir_contain){
												var root = '';
												if(i == 0){
													root = 'root';
												}
												*/
									html += '</tbody>';
										
									html += '</table>';
									html += '</div></div></div>';
						$('#data .default').html(html).show();
						$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
						
						
					}
				},
				error:function(){alert("error");}  

			});
			
			//alert(search_value);
		}
		else
		{
			html +='<tr class="not-found"><td>No matching records found</td>';
			html +='<td></td>';
			html +='<td></td>';
			html +='<td></td>';
			html +='<td></td>';
			html +='<td></td>';
			html +='<td></td>';
			html +='</tr></tbody>';
			$('#data .default').html(html).show();
			$('.files_datatable').dataTable({
										responsive: true,
										bPaginate: false,
										bInfo: false,
										bFilter:false
										
									});
		
		}
		
		
	});
	
	
	
	
	
	
	
	
	
	
	
	


});

									

	
	
</script>
