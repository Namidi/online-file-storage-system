<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

//$this->pageTitle=Yii::app()->name . ' - Login';
//$this->breadcrumbs=array(
//	'Login',
//);
?>

<!--<h1>Login</h1>-->

<!--<p>Please fill out the following form with your login credentials:</p>-->
<div class="form login_form middle-box text-center loginscreen  animated fadeInDown">
	<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
	
    ?>
	<div>
		<div>
			<h1 class="logo-name"></h1>
			<img style="width:340px;margin-left:-17px;" src="http://www.unasat.org/wp-content/uploads/2015/03/unasat_logo.png" />
		</div>
		<div style="height:10px;">
		
		</div>
		<h3>SCHOOL OF ICT</h3>
		<?php $errors = $model->getErrors(); ?>
		
				<span style="color:red;"><?php echo isset($errors['login'])?$errors['login'][0]:''; ?></span>
		<form class="m-t" role="form" action="index.html">
			<div class="form-group">
				<span style="color:red;"><?php echo $form->error($model, 'email'); ?></span>
				<?php echo $form->textField($model, 'email', array('placeholder'=>'Mail Address','class'=>'form-control')); ?>
				
			</div>
			<div class="form-group">
				<span style="color:red;"><?php echo $form->error($model, 'password'); ?></span>
				<?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
			</div>
			 <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-primary block full-width m-b')); ?>
			 <a href="<?php echo Yii::app()->baseUrl; ?>/index.php/forgotPassword/"><small>Forgot password?</small></a>
		</form>
	</div>
	<?php $this->endWidget();   ?>
</div>