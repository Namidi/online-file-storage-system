// hide log in button etc. when form fields not filled
	// commented out due to some browsers having issues with it
	// checkShowCredentials();
	// $('input#user, input#password').keyup(checkShowCredentials);

$( document ).ready(function() {                
	$('#settings #expand').keydown(function(event) {
		if (event.which === 13 || event.which === 32) {
			$('#expand').click()
		}
	});
	$('#settings #expand').click(function(event) {
		$('#settings #expanddiv').slideToggle(200);
		event.stopPropagation();
	});
	$('#settings #expanddiv').click(function(event){
		event.stopPropagation();
	});
	$(document).click(function(){//hide the settings menu when clicking outside it
		$('#settings #expanddiv').slideUp(200);
	});

	if( $.isFunction($.fn.inFieldLabels) ){
        $("p.label label").inFieldLabels({
            fadeOpacity:0
        });
    };
})